﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using AppInsured.Crosscutting.Configuration;

namespace AppInsured.Droid
{
	[Activity(Label = "CustomGoogleAuthInterceptor", NoHistory = true, LaunchMode = LaunchMode.SingleTop)]
	[IntentFilter(
		 new[] { Intent.ActionView },
		 Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
		 DataSchemes = new[] { "com.googleusercontent.apps.104827792629-r4q8njq0medi1fl0cipauaddne2mcja2" },
		 DataPath = "/oauth2redirect")]
	public class CustomGoogleAuthInterceptor : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Convert Android.Net.Url to Uri
			var uri = new Uri(Intent.Data.ToString());
			// Load redirectUrl page
			AuthenticationState.Authenticator.OnPageLoading(uri);

            var intent = new Intent(this, typeof(MainActivity));
            intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
            StartActivity(intent);

            Finish();
		}
	}
}
