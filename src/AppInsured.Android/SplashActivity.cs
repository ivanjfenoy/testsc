﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
namespace AppInsured.Droid
{
    [Activity(Icon = "@drawable/icon", LaunchMode = LaunchMode.SingleTask, MainLauncher = true, NoHistory = true, Theme = "@style/MyTheme.Splash", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }

        protected override void OnNewIntent(Intent intent)
        {
            this.Intent = intent;
        }
    }
}