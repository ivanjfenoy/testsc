using System;
using System.Net.Http;
using System.Net.Http.Headers;
using AppInsured.Crosscutting.Exceptions;

namespace AppInsured.Test
{
    public class GlobalSetting
    {
        public static HttpClient ClientIdentity = new HttpClient(new LoggingHandler(new HttpClientErrorHandler { }));
        public static HttpClient ClientInsured = new HttpClient(new LoggingHandler(new HttpClientErrorHandler { }));

        public const string ApplicationToken = "A98CD229-17A9-4F07-AF0D-63399B90FD31";

        //Facebook Config
        public const string FacebookLoginProvider = "FACEBOOK";
        public const string FacebookAppId = "258038865075355";
        public const string FacebookDisplayName = "San Cristóbal";
        public const string FacebookUrlApi = "https://graph.facebook.com/";

        //Google Config
        public const string GoogleLoginProvider = "GOOGLE";
        public const string GoogleDisplayName = "San Cristóbal";
        public static string iOSClientId = "104827792629-ql0ghi1bvkkj2ucke95r21epsopchgmu.apps.googleusercontent.com";
        public static string AndroidClientId = "104827792629-r4q8njq0medi1fl0cipauaddne2mcja2.apps.googleusercontent.com";
        public static string Scope = "https://www.googleapis.com/auth/userinfo.email";
        public static string AuthorizeUrl = "https://accounts.google.com/o/oauth2/v2/auth";
        public static string AccessTokenUrl = "https://www.googleapis.com/oauth2/v4/token";
        public static string UserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";
        public static string iOSRedirectUrl = "com.googleusercontent.apps.104827792629-ql0ghi1bvkkj2ucke95r21epsopchgmu:/oauth2redirect";
        public static string AndroidRedirectUrl = "com.googleusercontent.apps.104827792629-r4q8njq0medi1fl0cipauaddne2mcja2:/oauth2redirect";

        public GlobalSetting()
        {
            InitHttpClient();
        }

        public static GlobalSetting Instance { get; } = new GlobalSetting();

        private void InitHttpClient()
        {
            ClientIdentity.BaseAddress = new Uri(App.AppSettings.BaseIdentityUrl);
            ClientIdentity.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            ClientIdentity.Timeout = TimeSpan.FromMinutes(1);

            ClientInsured.BaseAddress = new Uri(App.AppSettings.BaseInsuredUrl);
            ClientInsured.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            ClientInsured.Timeout = TimeSpan.FromMinutes(1);
        }
    }
}
