﻿using AppInsured.Services.Settings;
using AppInsured.ViewModels;
using AppInsured.ViewModels.Base;
using AutoFixture;
using Autofac.Extras.Moq;
using NUnit.Framework;
using Xamarin.Forms;
using AppInsured.Bootstrap;
using AppCoreSDK.Models.Auth;
using FluentAssertions;

namespace AppInsured.Test.ViewModels
{
    public class BaseViewModelTest
    {
        ViewModelBase mockViewModel;

        [SetUp]
        public void Setup()
        {
            Xamarin.Forms.Mocks.MockForms.Init();
            AppContainer.InitializeContainer();
            ViewModelLocator.UpdateDependencies(true);
            Application.Current = new App();

            using (var mock = AutoMock.GetLoose())
            {
                mockViewModel = mock.Create<ViewModelBase>();
            }
        }

        [Test]
        public void ViewModelBase_PagePropertyNull()
        {
            mockViewModel.Page.Should().BeNull();
        }

        [Test]
        public void ViewModelBase_TitlePropertyNotNull()
        {
            mockViewModel.Title = "Welcome";
            mockViewModel.Title.Should().NotBeNullOrEmpty();
        }
    }
}
