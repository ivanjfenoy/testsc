﻿using System;
using System.Threading.Tasks;
using AppInsured.Services.Dialog;
using AppInsured.Services.RestService;
using AppInsured.Services.Settings;
using AppInsured.ViewModels;
using AppInsured.ViewModels.Base;
using Moq;
using AutoFixture;
using Autofac.Extras.Moq;
using NUnit.Framework;
using FluentAssertions;
using Xamarin.Forms;
using AppInsured.Services.Insured.Auth;
using AppInsured.Bootstrap;
using AppCoreSDK.Models.Auth;
using AppInsured.Services.Navigation;

namespace AppInsured.Test.ViewModels
{
    [TestFixture]
    public class HomeViewModelTest
    {
        HomeViewModel mockViewModel;
        ISettingsService settingsServiceMock;
        private Fixture _fixture;

        [SetUp]
        public void Setup()
        {
            Xamarin.Forms.Mocks.MockForms.Init();
            AppContainer.InitializeContainer();
            ViewModelLocator.UpdateDependencies(true);
            _fixture = new Fixture();

            Application.Current = new App();
            //settingsServiceMock = new Mock<ISettingsService>().Object;
            //var dialogServiceMock = new Mock<IDialogService>().Object;
            //var authClientMock = new Mock<IAuthService>().Object;
            //var socialClientMock = new Mock<ISocialNetworkService>().Object;
            //var asd = GlobalSetting.Instance;
            //mockViewModel = new HomeViewModel(settingsServiceMock, dialogServiceMock, socialClientMock, authClientMock);
            using (var mock = AutoMock.GetLoose())
            {
                mockViewModel = mock.Create<HomeViewModel>();
            }
        }

        [Test]
        public void CreatingHomeViewModel_InitializesOpenSignUpCommand()
        {
            mockViewModel.OpenSignUpCommand.Should().NotBeNull();
            mockViewModel.OpenSignUpCommand.Should().BeOfType<Command>();
        }

        [Test]
        public void CreatingHomeViewModel_InitializesCallLoginCommand()
        {
            mockViewModel.CallLoginCommand.Should().NotBeNull();
            mockViewModel.CallLoginCommand.Should().BeOfType<Command>();
        }

        [Test]
        public void CreatingHomeViewModel_InitializesOnFacebookLoginSuccessCmd()
        {
            mockViewModel.OnFacebookLoginSuccessCmd.Should().NotBeNull();
            mockViewModel.OnFacebookLoginSuccessCmd.Should().BeOfType<Command<string>>();
        }

        [Test]
        public void CreatingHomeViewModel_InitializesOnFacebookLoginErrorCmd()
        {
            mockViewModel.OnFacebookLoginErrorCmd.Should().NotBeNull();
            mockViewModel.OnFacebookLoginErrorCmd.Should().BeOfType<Command<string>>();
        }

        [Test]
        public void CreatingHomeViewModel_InitializesOnGoogleLoginCmd()
        {
            mockViewModel.OnGoogleLoginCmd.Should().NotBeNull();
            mockViewModel.OnGoogleLoginCmd.Should().BeOfType<Command>();
        }

        [Test]
        public void CreatingHomeViewModel_AuthenticatorIsNotNull()
        {
            mockViewModel.Authenticator.Should().NotBeNull();
        }

        [Test]
        public void CreatingHomeViewModel_CoreAppIsNull()
        {
            mockViewModel.CoreApp.Should().BeNull();
        }

        [Test]
        public async Task CreatingHomeViewModel_LoginError()
        {
            using (var mock = AutoMock.GetLoose())
            {
                mockViewModel.ForgotSession();
                await mock.Mock<INavigationService>().Object.NavigateToAsync<MainViewModel>();
            }
        }

        [Test]
        public async Task CreatingHomeViewModel_LoginCompleted()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var session = _fixture.Create<SessionUser>();
                mockViewModel.RememberSession(session);
                await mock.Mock<INavigationService>().Object.NavigateToAsync<MainViewModel>();
            }
        }

        [Test]
        public async Task CreatingHomeViewModel_OpenHelp()
        {
            using (var mock = AutoMock.GetLoose())
            {
                await mock.Mock<IDialogService>().Object.ShowAlertAsync("Test Message", "APP", "OK");
                await mock.Mock<INavigationService>().Object.NavigateToAsync<MainViewModel>();
            }
        }

        [Test]
        public async Task CreatingHomeViewModel_InitLoginGoogle()
        {
            mockViewModel.InitLoginGoogle();
        }

    }
}
