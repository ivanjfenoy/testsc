﻿using System;
using System.Threading.Tasks;
using AppInsured.Services.Dialog;
using AppInsured.Services.RestService;
using AppInsured.Services.Settings;
using AppInsured.ViewModels;
using AppInsured.ViewModels.Base;
using Moq;
using AutoFixture;
using Autofac.Extras.Moq;
using NUnit.Framework;
using FluentAssertions;
using Xamarin.Forms;
using AppInsured.Services.Insured.Auth;
using AppInsured.Bootstrap;
using AppCoreSDK.Models.Auth;
using AppInsured.Services.Navigation;
using Insured.Api.Sdk;
using AppInsured.Models;
using System.Linq;
using System.Collections.ObjectModel;

namespace AppInsured.Test.ViewModels
{
    [TestFixture]
    public class LandingViewModelTest
    {
        LandingViewModel mockViewModel;
        private Fixture _fixture;

        [SetUp]
        public void Setup()
        {
            Xamarin.Forms.Mocks.MockForms.Init();
            AppContainer.InitializeContainer();
            ViewModelLocator.UpdateDependencies(true);
            _fixture = new Fixture();
            Application.Current = new App();

            using (var mock = AutoMock.GetLoose())
            {
                var policiyMock = mock.Mock<IPolicyClient>().Object;
                var settingMock = mock.Create<SettingsService>();
                var session = new SessionUser()
                {
                    AuthorizationToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBsaWNhdGlvbklkIjoiMiIsIkFwcGxpY2F0aW9uVG9rZW4iOiJBOThDRDIyOS0xN0E5LTRGMDctQUYwRC02MzM5OUI5MEZEMzEiLCJBdXRvbWF0aW9uQWNjZXNzIjoiVHJ1ZSIsIm5hbWVpZCI6ImZkZmIwOWFhLTg2N2EtNDEwNy1iMTA2LTAwOTZhZWMwMzFlYSIsInVuaXF1ZV9uYW1lIjoidXNlckBleGFtcGxlc2FkYXNkMTc1NzQ1LmNvbSIsImVtYWlsIjoidXNlckB0ZXN0aW5nMTIzNC5jb20iLCJGaXJzdE5hbWUiOiJSb2RyaWdvIiwiTGFzdE5hbWUiOiJNYXJ0aW5leiIsIlRheElkIjoiMzEyODAyODkiLCJPZmZpY2lhbElkVHlwZSI6IkV4dF9ETkk5NiIsImdlbmRlciI6Ik0iLCJDdWl0Q3VpbCI6IjIzMzEyODAyODk5IiwiQ3JlYXRlZERhdGUiOiIyMDIwLTEyLTI0IDEyOjE3OjI4IiwibW9iaWxlcGhvbmUiOiIxMTE1MDAwMDAwMTA1Iiwicm9sZSI6WyJBY2Nlc3NBcHBsaWNhdGlvbiIsIklkZW50aXR5VmVyaWZ5Il0sIm5iZiI6MTYyMTQ0MDAyMCwiZXhwIjoxNjIxNDQ3MjIwLCJpYXQiOjE2MjE0NDAwMjB9.G7aHH-wQ9UYjFzJ20vYvntdzEzQTfuyP3u9awNhhhY4",
                    RefreshToken = "Eg/UdIV/kFRoBr9rxJBeLT+CoGZKaNdpsVfnlLbRX8U="
                };
                settingMock.Session = session;
                mockViewModel = new LandingViewModel(policiyMock, settingMock);
            }
        }

        [Test]
        public void CreatingHomeViewModel_HasCarFalse()
        {
            mockViewModel.HasCar.Should().BeFalse();
        }

        [Test]
        public void CreatingHomeViewModel_HasCarTrue()
        {
            mockViewModel.HasCar = _fixture.CreateMany<PolicyDTO>(10).ToList().Count > 0;
            mockViewModel.HasCar.Should().BeTrue();
        }

        [Test]
        public void CreatingHomeViewModel_HeightPolicyEquals0()
        {
            mockViewModel.HeightPolicy.Should().BeLessOrEqualTo(0);
        }

        [Test]
        public void CreatingHomeViewModel_HeightPolicyEquals200()
        {
            mockViewModel.Policies = new ObservableCollection<PolicyDTO>(_fixture.CreateMany<PolicyDTO>(10));
            mockViewModel.HeightPolicy.Should().BeLessOrEqualTo(200);
        }

        [Test]
        public void CreatingHomeViewModel_HeightPolicyOtherEquals0()
        {
            mockViewModel.HeightOtherPolicy.Should().BeLessOrEqualTo(0);
        }

        [Test]
        public void CreatingHomeViewModel_HeightOtherPolicyEquals40()
        {
            mockViewModel.OtherPolicies = new ObservableCollection<OtherPolicy>(_fixture.CreateMany<OtherPolicy>(10));
            mockViewModel.HeightOtherPolicy.Should().BeLessOrEqualTo(200);
        }

        [Test]
        public void CreatingHomeViewModel_PoliciesBeNull()
        {
            mockViewModel.Policies.Should().BeNullOrEmpty();
        }

        [Test]
        public void CreatingHomeViewModel_PoliciesBeNotNull()
        {
            mockViewModel.Policies = new ObservableCollection<PolicyDTO>(_fixture.CreateMany<PolicyDTO>(10));
            mockViewModel.Policies.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void CreatingHomeViewModel_OtherPoliciesBeNull()
        {
            mockViewModel.OtherPolicies.Should().BeNullOrEmpty();
        }

        [Test]
        public void CreatingHomeViewModel_OtherPoliciesBeNotNull()
        {
            mockViewModel.OtherPolicies = new ObservableCollection<OtherPolicy>(_fixture.CreateMany<OtherPolicy>(10));
            mockViewModel.OtherPolicies.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void CreatingHomeViewModel_CarouselBeNull()
        {
            mockViewModel.Carousel = null;
            mockViewModel.Carousel.Should().BeNullOrEmpty();
        }

        [Test]
        public void CreatingHomeViewModel_CarouselBeNotNull()
        {
            mockViewModel.Carousel.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void CreatingHomeViewModel_OptionCommand()
        {
            mockViewModel.OptionCommand.Should().NotBeNull();
            mockViewModel.OptionCommand.Should().BeOfType<Command<string>>();
        }

        [Test]
        public void CreatingHomeViewModel_OpenOptions()
        {
            mockViewModel.OptionCommand.Execute("accidente");
        }

        //TESTS FOR VIEWMODELBASE ----------------------------------------------

        [Test]
        public void ViewModelBase_PagePropertyNull()
        {
            mockViewModel.Page.Should().BeNull();
        }

        [Test]
        public void ViewModelBase_TitlePropertyNotNull()
        {
            mockViewModel.Title.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void ViewModelBase_IsBusyPropertyFalse()
        {
            mockViewModel.IsBusy.Should().BeFalse();
        }

        [Test]
        public void ViewModelBase_IsBusyPropertyTrue()
        {
            mockViewModel.BusyAsync(async () =>
            {
                mockViewModel.IsBusy.Should().BeTrue();
            }, "", false);
        }

        [Test]
        public void ViewModelBase_BusyMessagePropertyEmpty()
        {
            mockViewModel.BusyMessage.Should().BeNullOrEmpty();
        }

        [Test]
        public void ViewModelBase_BusyMessagePropertyNotEmpty()
        {
            mockViewModel.BusyAsync(async () =>
            {
                mockViewModel.BusyMessage.Should().NotBeNullOrEmpty();
            }, "Message example", false);
        }

        [Test]
        public void ViewModelBase_RememberSession()
        {
            mockViewModel.RememberSession(_fixture.Create<SessionUser>());
        }
    }
}
