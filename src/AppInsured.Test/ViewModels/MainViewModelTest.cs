﻿using AppInsured.Services.Settings;
using AppInsured.ViewModels;
using AppInsured.ViewModels.Base;
using AutoFixture;
using Autofac.Extras.Moq;
using NUnit.Framework;
using Xamarin.Forms;
using AppInsured.Bootstrap;
using AppCoreSDK.Models.Auth;

namespace AppInsured.Test.ViewModels
{
    [TestFixture]
    public class MainViewModelTest
    {
        MainViewModel mockViewModel;
        private Fixture _fixture;

        [SetUp]
        public void Setup()
        {
            Xamarin.Forms.Mocks.MockForms.Init();
            AppContainer.InitializeContainer();
            ViewModelLocator.UpdateDependencies(true);
            _fixture = new Fixture();
            Application.Current = new App();

            using (var mock = AutoMock.GetLoose())
            {
                var settingMock = mock.Create<SettingsService>();
                var session = new SessionUser()
                {
                    AuthorizationToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBsaWNhdGlvbklkIjoiMiIsIkFwcGxpY2F0aW9uVG9rZW4iOiJBOThDRDIyOS0xN0E5LTRGMDctQUYwRC02MzM5OUI5MEZEMzEiLCJBdXRvbWF0aW9uQWNjZXNzIjoiVHJ1ZSIsIm5hbWVpZCI6ImZkZmIwOWFhLTg2N2EtNDEwNy1iMTA2LTAwOTZhZWMwMzFlYSIsInVuaXF1ZV9uYW1lIjoidXNlckBleGFtcGxlc2FkYXNkMTc1NzQ1LmNvbSIsImVtYWlsIjoidXNlckB0ZXN0aW5nMTIzNC5jb20iLCJGaXJzdE5hbWUiOiJSb2RyaWdvIiwiTGFzdE5hbWUiOiJNYXJ0aW5leiIsIlRheElkIjoiMzEyODAyODkiLCJPZmZpY2lhbElkVHlwZSI6IkV4dF9ETkk5NiIsImdlbmRlciI6Ik0iLCJDdWl0Q3VpbCI6IjIzMzEyODAyODk5IiwiQ3JlYXRlZERhdGUiOiIyMDIwLTEyLTI0IDEyOjE3OjI4IiwibW9iaWxlcGhvbmUiOiIxMTE1MDAwMDAwMTA1Iiwicm9sZSI6WyJBY2Nlc3NBcHBsaWNhdGlvbiIsIklkZW50aXR5VmVyaWZ5Il0sIm5iZiI6MTYyMTQ0MDAyMCwiZXhwIjoxNjIxNDQ3MjIwLCJpYXQiOjE2MjE0NDAwMjB9.G7aHH-wQ9UYjFzJ20vYvntdzEzQTfuyP3u9awNhhhY4",
                    RefreshToken = "Eg/UdIV/kFRoBr9rxJBeLT+CoGZKaNdpsVfnlLbRX8U="
                };
                settingMock.Session = session;
                mockViewModel = new MainViewModel(settingMock);
            }
        }

        [Test]
        public void CreatingHomeViewModel_GetPage()
        {
            mockViewModel.GetPage();
        }
    }
}
