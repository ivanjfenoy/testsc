﻿using AppInsured.Services.Settings;
using AppInsured.ViewModels;
using AppInsured.ViewModels.Base;
using AutoFixture;
using Autofac.Extras.Moq;
using NUnit.Framework;
using Xamarin.Forms;
using AppInsured.Bootstrap;
using AppCoreSDK.Models.Auth;

namespace AppInsured.Test.ViewModels
{
    public class SplashViewModelTest
    {
        SplashViewModel mockViewModel;

        [SetUp]
        public void Setup()
        {
            Xamarin.Forms.Mocks.MockForms.Init();
            AppContainer.InitializeContainer();
            ViewModelLocator.UpdateDependencies(true);
            Application.Current = new App();

            using (var mock = AutoMock.GetLoose())
            {
                mockViewModel = mock.Create<SplashViewModel>();
            }
        }

        [Test]
        public void CreatingHomeViewModel_SetMain()
        {
            mockViewModel.SetMain();
        }

    }
}
