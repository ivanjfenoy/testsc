﻿using System;
using System.IO;
using System.Linq;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AppInsured.UITest
{
	public class AppInitializer
	{
		public static IApp StartApp(Platform platform)
		{
			if (platform == Platform.Android)
			{
				return ConfigureApp
					.Android
					.ApkFile("../../../AppInsured.Android/bin/Release/com.sancristobal.asegurados.apk")
					.WaitTimes(new WaitTimes())
					.StartApp();
			}

			return ConfigureApp
				.iOS
				.StartApp();
		}
	}
}
