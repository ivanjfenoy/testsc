﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using AppInsured.Crosscutting.Configuration;
using AppInsured.ViewModels;
using Facebook.CoreKit;
using Foundation;
using UIKit;
using Xamarin.Forms;

namespace AppInsured.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            #if ENABLE_TEST_CLOUD
            Xamarin.Calabash.Start();
            #endif
            Rg.Plugins.Popup.Popup.Init();

            global::Xamarin.Forms.Forms.Init();

            XamEffects.iOS.Effects.Init();
            // create a new window instance based on the screen size
            Profile.EnableUpdatesOnAccessTokenChange(true);
            Facebook.CoreKit.Settings.AppId = GlobalSetting.FacebookAppId;
            Facebook.CoreKit.Settings.DisplayName = GlobalSetting.FacebookDisplayName;

            AiForms.Effects.iOS.Effects.Init();

            global::Xamarin.Auth.Presenters.XamarinIOS.AuthenticationConfiguration.Init();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            // Convert NSUrl to Uri
            var uri = new Uri(url.AbsoluteString);
            if (uri.Scheme.Equals("sancristobalapp"))
            {
                HandleScDeeplink(uri);
            }

            // Load redirectUrl page
            if (AuthenticationState.Authenticator != null) AuthenticationState.Authenticator.OnPageLoading(uri);

            return ApplicationDelegate.SharedInstance.OpenUrl(app, url, options);
        }
        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            // We need to handle URLs by passing them to their own OpenUrl in order to make the SSO authentication works.
            return ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
        }

        public void HandleScDeeplink(Uri uri)
        {
            switch (uri.Host)
            {
                case "recuperar-contrasena":
                    MessagingCenter.Send<object, Uri>(this, "recover_pass", uri);
                    break;
                case "activar-cuenta":
                    MessagingCenter.Send<object, Uri>(this, "activate_account", uri);
                    break;
            }
        }

    }
}
