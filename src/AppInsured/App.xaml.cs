﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using AppCoreSDK.Models;
using AppInsured.Bootstrap;
using AppInsured.Configuration;
using AppInsured.Resources;
using AppInsured.Services.Navigation;
using AppInsured.Services.Settings;
using AppInsured.ViewModels.Base;
using AppInsured.Views;
using AutoMapper;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter.Distribute;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace AppInsured
{
    public partial class App : Application
    {
        private static AppSettings appSettings;
        private ISettingsService _settingService;
        public static IMapper mapper;
        public static AppSettings AppSettings
        {
            get
            {
                if (appSettings == null)
                    LoadAppSettings();
                return appSettings;
            }
        }

        public static AppContainer Container { get; set; }

        public App()
        {
            InitializeComponent();

            Container = new AppContainer();

            LoadAppSettings();
            InitApp();
        }

        private void InitApp()
        {
            _settingService = Container.Resolve<ISettingsService>();

            _settingService.DeepLink = string.Empty;

            CultureInfo spanishESCulture = new CultureInfo("es-ES");
            CultureInfo.DefaultThreadCurrentCulture = spanishESCulture;
            try
            {
                Microsoft.AppCenter.AppCenter.Start(appSettings.AppCenterSecretAndroid + appSettings.AppCenterSecretiOS,
                typeof(Analytics), typeof(Crashes), typeof(Distribute));
                Analytics.SetEnabledAsync(appSettings.AppCenterAnalytics);
                Crashes.SetEnabledAsync(appSettings.AppCenterCrashes);
                Distribute.SetEnabledAsync(appSettings.AppCenterDistribute);
            }
            catch (Exception ex) { }

            mapper = AppMapper.CreateMapper();
        }

        private Task InitNavigation()
        {
            var navigationService = Container.Resolve<INavigationService>();
            return navigationService.InitializeAsync();
        }

        protected override async void OnStart()
        {
            await InitNavigation();

            MessagingCenter.Unsubscribe<object, Uri>(this, "recover_pass");
            MessagingCenter.Subscribe<object, Uri>(this, "recover_pass", (sender, arg) =>
            {
                if (arg != null)
                {
                    _settingService.DeepLink = arg.AbsoluteUri;
                }
            });
            MessagingCenter.Unsubscribe<object, Uri>(this, "activate_account");
            MessagingCenter.Subscribe<object, Uri>(this, "activate_account", (sender, arg) =>
            {
                if (arg != null)
                {
                    _settingService.DeepLink = arg.AbsoluteUri;
                }
            });

            base.OnResume();
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        protected override void OnAppLinkRequestReceived(Uri uri)
        {
            if (uri.Host.Contains("recuperar-contrasena", StringComparison.OrdinalIgnoreCase))
            {
                _settingService.DeepLink = uri.AbsoluteUri;
            }
            else if(uri.Host.Contains("activar-cuenta", StringComparison.OrdinalIgnoreCase))
            {
                _settingService.DeepLink = uri.AbsoluteUri;
            }
        }

        bool OnReleaseAvailable(ReleaseDetails releaseDetails)
        {
            // Look at releaseDetails public properties to get version information, release notes text or release notes URL
            string versionName = releaseDetails.ShortVersion;
            string versionCodeOrBuildNumber = releaseDetails.Version;
            string releaseNotes = releaseDetails.ReleaseNotes;
            Uri releaseNotesUrl = releaseDetails.ReleaseNotesUrl;

            // custom dialog
            var title = "La versión " + versionName + " ya esta disponible!";
            Task answer;

            // On mandatory update, user cannot postpone
            if (releaseDetails.MandatoryUpdate)
            {
                answer = Current.MainPage.DisplayAlert(title, releaseNotes, "Descargar e instalar");
            }
            else
            {
                answer = Current.MainPage.DisplayAlert(title, releaseNotes, "Descargar e instalar", "Más tarde...");
            }
            answer.ContinueWith((task) =>
            {
                // If mandatory or if answer was positive
                if (releaseDetails.MandatoryUpdate || (task as Task<bool>).Result)
                {
                    // Notify SDK that user selected update
                    Distribute.NotifyUpdateAction(UpdateAction.Update);
                }
                else
                {
                    // Notify SDK that user selected postpone (for 1 day)
                    // Note that this method call is ignored by the SDK if the update is mandatory
                    Distribute.NotifyUpdateAction(UpdateAction.Postpone);
                }
            });

            // Return true if you are using your own dialog, false otherwise
            return true;
        }
       
        #region Environment Settings
        private static void LoadAppSettings()
        {
#if DEV
            var appSettingResourceStream = Assembly.GetAssembly(typeof(AppSettings)).GetManifestResourceStream("AppInsured.Configuration.appsettings.dev.json");
#elif QA
            var appSettingResourceStream = Assembly.GetAssembly(typeof(AppSettings)).GetManifestResourceStream("AppInsured.Configuration.appsettings.QA.json");
#elif UAT
            var appSettingResourceStream = Assembly.GetAssembly(typeof(AppSettings)).GetManifestResourceStream("AppInsured.Configuration.appsettings.UAT.json");
#elif PROD
            var appSettingResourceStream = Assembly.GetAssembly(typeof(AppSettings)).GetManifestResourceStream("AppInsured.Configuration.appsettings.prod.json");
#else
            var appSettingResourceStream = Assembly.GetAssembly(typeof(AppSettings)).GetManifestResourceStream("AppInsured.Configuration.appsettings.QA.json");
#endif
            if (appSettingResourceStream == null)
                return;

            using (var streamreader = new StreamReader(appSettingResourceStream))
            {
                var jsonString = streamreader.ReadToEnd();
                appSettings = JsonConvert.DeserializeObject<AppSettings>(jsonString);
            }
        }

        public static string Env()
        {
#if DEV
            return StringResources.DevEnv;
#elif QA
            return StringResources.QAEnv;
#elif UAT
            return StringResources.UATEnv;
#else
            return string.Empty;
#endif
        }
        #endregion
    }
}
