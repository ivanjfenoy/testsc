﻿using System;
using AppInsured.Models;
using AutoMapper;
using Insured.Api.Dto.Queries;

namespace AppInsured
{
    public static class AppMapper
    {
        public static IMapper CreateMapper()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Policy, PolicyDTO>();
            });
            return mapperConfiguration.CreateMapper();
        }
    }
}
