﻿using System;
using Xamarin.Forms;

namespace AppInsured.Behaviors
{
    public class EntryOnlyNumberBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += OnEntryTextChanged;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= OnEntryTextChanged;
        }

        void OnEntryTextChanged(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;

            //if (entry.Text.Length > this.MaxLength)
            //{
            //    string entryText = entry.Text;
            //    entry.TextChanged -= OnEntryTextChanged;
            //    entry.Text = e.OldTextValue;
            //    entry.TextChanged += OnEntryTextChanged;
            //}
        }
    }
}
