﻿using System;
using AppInsured.Services.Dependency;
using AppInsured.Services.Dialog;
using AppInsured.Services.Insured.Auth;
using AppInsured.Services.Insured.Policy;
using AppInsured.Services.Media;
using AppInsured.Services.Navigation;
using AppInsured.Services.RestService;
using AppInsured.Services.Settings;
using AppInsured.ViewModels;
using Autofac;
using AutoMapper;
using Insured.Api.Sdk;

namespace AppInsured.Bootstrap
{
    public class AppContainer
    {
        private static IContainer container;

        public AppContainer()
        {
            // services
            var builder = new ContainerBuilder();
            builder.RegisterType<NavigationService>().As<INavigationService>().SingleInstance();
            builder.RegisterType<DialogService>().As<IDialogService>().SingleInstance();
            builder.RegisterType<SettingsService>().As<ISettingsService>().SingleInstance();
            builder.RegisterType<Services.Dependency.DependencyService>().As<IDependencyService>().SingleInstance();
            builder.RegisterType<MediaService>().As<IMediaService>().SingleInstance();
            builder.RegisterType<SocialNetworkService>().As<ISocialNetworkService>().SingleInstance();
            builder.RegisterType<PolicyService>().As<IPolicyClient>().SingleInstance();
            builder.RegisterType<AuthService>().As<IAuthService>().SingleInstance();
            builder.RegisterType<Mapper>().As<IMapper>().SingleInstance();

            // view models
            builder.RegisterType<HomeViewModel>().SingleInstance();
            builder.RegisterType<MainViewModel>().SingleInstance();
            builder.RegisterType<LandingViewModel>().SingleInstance();
            builder.RegisterType<SplashViewModel>().SingleInstance();

            container = builder.Build();
        }

        public T Resolve<T>() => container.Resolve<T>();

        public object Resolve(Type type) => container.Resolve(type);

        public static void InitializeContainer()
        {
            App.Container = new AppContainer();
        }
    }
}
