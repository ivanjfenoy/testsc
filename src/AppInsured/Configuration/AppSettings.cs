﻿using System;
namespace AppInsured.Configuration
{
    public class AppSettings
    {
        public string BaseApiMobileUrl { get; set; }
        public string BaseIdentityUrl { get; set; }
        public string BaseInsuredUrl { get; set; }
        public string BaseMarketingUrl { get; set; }
        public string AppCenterSecretAndroid { get; set; }
        public string AppCenterSecretiOS { get; set; }
        public bool AppCenterAnalytics { get; set; }
        public bool AppCenterCrashes { get; set; }
        public bool AppCenterDistribute { get; set; }
        public string ApiKey { get; set; }
        public string OneSignalAPiKey { get; set; }
    }
}
