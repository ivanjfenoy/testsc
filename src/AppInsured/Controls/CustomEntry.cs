﻿using System.Windows.Input;
using Xamarin.Forms;

namespace AppInsured.CustomControls
{
    public class CustomEntry : Entry
    {
        public static BindableProperty CompletedCommandProperty = BindableProperty.Create<CustomEntry, ICommand>(o => o.CompletedCommand, default(ICommand));

        public ICommand CompletedCommand
        {
            get { return (ICommand)GetValue(CompletedCommandProperty); }
            set { SetValue(CompletedCommandProperty, value); }
        }
        public CustomEntry()
        {
            this.HeightRequest = (Device.OS == TargetPlatform.iOS) ? 40 : 45;
            this.Unfocused += (s, e) => CompletedCommand?.Execute(this.Text);
        }

    }
}
