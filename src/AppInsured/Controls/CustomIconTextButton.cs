﻿using Xamarin.Forms;

namespace AppInsured.CustomControls
{
    public class CustomIconTextButton : Button
    {
        public static BindableProperty IconProperty = BindableProperty.Create(
                                                        propertyName: "Icon",
                                                        returnType: typeof(string),
                                                        declaringType: typeof(CustomIconTextButton),
                                                        defaultValue: "",
                                                        defaultBindingMode: BindingMode.TwoWay,
                                                        propertyChanged: IconPropertyChanged);

        public string Icon
        {
            get { return base.GetValue(IconProperty).ToString(); }
            set { base.SetValue(IconProperty, value); }
        }

        private static void IconPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (CustomIconTextButton)bindable;
            control.Text = (control.TextButton.Length > 0) ? newValue.ToString() + "  " + control.TextButton : newValue.ToString();
            control.FontFamily = "FontAwesome";
        }

        public static BindableProperty TextButtonProperty = BindableProperty.Create(
                                                        propertyName: "TextButton",
                                                        returnType: typeof(string),
                                                        declaringType: typeof(CustomIconTextButton),
                                                        defaultValue: "",
                                                        defaultBindingMode: BindingMode.TwoWay,
                                                        propertyChanged: TextPropertyChanged);

        public string TextButton
        {
            get { return base.GetValue(TextButtonProperty).ToString(); }
            set { base.SetValue(TextButtonProperty, value); }
        }

        private static void TextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (CustomIconTextButton)bindable;
            control.Text = (control.Icon.Length > 0) ? control.Icon + "&#x0a;" + newValue.ToString() : newValue.ToString();
            control.HorizontalOptions = LayoutOptions.Center;
        }

        public CustomIconTextButton()
        {

        }
    }
}
