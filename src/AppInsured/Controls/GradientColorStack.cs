﻿using AppInsured.Resources;
using Xamarin.Forms;

namespace AppInsured.CustomControls
{
    public class GradientColorStack : StackLayout
    {
        public static BindableProperty StartColorProperty = BindableProperty.Create(
                                                        propertyName: "StartColor",
                                                        returnType: typeof(Color),
                                                        declaringType: typeof(GradientColorStack),
                                                        defaultValue: ColorResources.PrimaryColor,
                                                        defaultBindingMode: BindingMode.TwoWay,
                                                        propertyChanged: StartColorPropertyChanged);

        public Color StartColor
        {
            get { return (Color)base.GetValue(StartColorProperty); }
            set { base.SetValue(StartColorProperty, value); OnPropertyChanged(); }
        }

        private static void StartColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (GradientColorStack)bindable;
            control.StartColor = (Color)newValue;
        }

        public static BindableProperty EndColorProperty = BindableProperty.Create(
                                                        propertyName: "EndColor",
                                                        returnType: typeof(Color),
                                                        declaringType: typeof(GradientColorStack),
                                                        defaultValue: ColorResources.PrimaryColorLight,
                                                        defaultBindingMode: BindingMode.TwoWay,
                                                        propertyChanged: EndColorPropertyChanged);

        public Color EndColor
        {
            get { return (Color)base.GetValue(EndColorProperty); }
            set { base.SetValue(EndColorProperty, value); OnPropertyChanged(); }
        }

        private static void EndColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (GradientColorStack)bindable;
            control.EndColor = (Color)newValue;
        }
    }
}
