﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace AppInsured.Converters
{
    public class StringUpperCaseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
          return ((string)parameter).ToUpper();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((string)parameter).ToLower();
        }
    }
}
