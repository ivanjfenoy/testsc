﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Core.Sdk.Lib;
using Newtonsoft.Json;

namespace AppInsured.Crosscutting.Exceptions
{
    public class HttpClientErrorHandler : DelegatingHandler
    {

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (InnerHandler == null)
                InnerHandler = new LoggingHandler(new HttpClientHandler());

            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            if (!response.IsSuccessStatusCode)
            {
                string failureReason = string.Empty;
                string content = string.Empty;

                try
                {
                    content = await response.Content.ReadAsStringAsync();

                    FailedResponse failedResponse = JsonConvert
                        .DeserializeObject<FailedResponse>(content);

                    failureReason = failedResponse?.FailureReason ??
                        (!string.IsNullOrWhiteSpace(content) ? content : response.ReasonPhrase);
                }
                catch
                {
                    failureReason = !string.IsNullOrWhiteSpace(content) ?
                        content : response.ReasonPhrase;
                }
                throw new RestApiException(failureReason, response.StatusCode);
            }

            return response;
        }
    }
}
