﻿using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AppInsured.Services.Dependency;
using AppInsured.ViewModels.Base;
using Xamarin.Forms;
using AppInsured.Crosscutting.Logs;

namespace AppInsured.Crosscutting.Exceptions
{
    public class LoggingHandler : DelegatingHandler
    {
        protected readonly ILogger Logger;

        public LoggingHandler(HttpMessageHandler innerHandler)
        : base(innerHandler)
        {

            if (Device.RuntimePlatform == Device.Android)
            {
                //var wDependencyService = ViewModelLocator.Resolve<IDependencyService>();
                //Logger = wDependencyService.Get<ILogManager>().GetLog();
            }
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Debug.WriteLine("------------------------------------------------------------------------------------");
            Debug.WriteLine("Request -->");
            Debug.WriteLine(request.ToString());
            if (request.Content != null)
            {
                Debug.WriteLine(await request.Content.ReadAsStringAsync());
            }
            Debug.WriteLine("");

            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            Debug.WriteLine("Response -->");
            Debug.WriteLine(response.ToString());
            if (response.Content != null)
            {
                Debug.WriteLine(await response.Content.ReadAsStringAsync());
            }
            Debug.WriteLine("------------------------------------------------------------------------------------");
#if PROD
#else
            if (Logger != null)
            {
                Logger.Error("------------------------------------------------------------------------------------");
                Logger.Error("Request -->");
                Logger.Error(request.ToString());
                if (request.Content != null)
                {
                    Logger.Error(await request.Content.ReadAsStringAsync());
                }

                Logger.Error("Response -->");
                Logger.Error(response.ToString());
                if (response.Content != null)
                {
                    Logger.Error(await response.Content.ReadAsStringAsync());
                }
                Logger.Error("------------------------------------------------------------------------------------");
            }
#endif
            return response;
        }
    }
}
