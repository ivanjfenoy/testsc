﻿using System;
namespace AppInsured.Crosscutting.Identifiers
{
    public static class Branch
    {
        public static int Car { get { return 1; } }
        public static int Fire { get { return 2; } }
        public static int Other { get { return 3; } }
        public static int Theft { get { return 4; } }
        public static int PersonalAccident { get { return 6; } }
        public static int Consortium { get { return 7; } }
        public static int CivilLiability { get { return 11; } }
        public static int Transport { get { return 12; } }
        public static int Boat { get { return 13; } }
        public static int Hail { get { return 14; } }
        public static int Technical { get { return 15; } }
        public static int Caution { get { return 16; } }
        public static int Life { get { return 17; } }
        public static int Bike { get { return 21; } }
    }
}
