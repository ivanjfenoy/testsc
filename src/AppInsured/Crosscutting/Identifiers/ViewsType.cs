﻿using System;
namespace AppInsured.Crosscutting.Identifiers
{
    public static class ViewsType
    {
        public const string Medic = "Medico";
        public const string Referred = "Referido";
        public const string Tourism = "Turismo";
        public const string Retirement = "Retiro";
        public const string Finance = "Finanza";
    }
}
