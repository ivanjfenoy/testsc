﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppInsured.Models
{
    public class AppVersion
    {
        public string AndroidVersion { get; set; }
        public string IOSVersion { get; set; }
        public bool StatusServer { get; set; }
    }
}
