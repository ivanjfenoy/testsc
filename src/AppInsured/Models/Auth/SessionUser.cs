﻿using System;
using AppInsured.Models.Response;

namespace AppInsured.Models.Auth
{
    public class SessionUser
    {
        public string RefreshToken { get; set; }
        public RestApiError Error { get; set; }
        public string AuthorizationToken { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string SocialNetworkToken { get; set; }
    }
}
