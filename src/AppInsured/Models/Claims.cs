﻿using System;
using System.Collections.Generic;

namespace AppInsured.Models
{
    public class Claims
    {

        public string ApplicationId { get; set; }
        public string ApplicationToken { get; set; }
        public string AutomationAccess { get; set; }
        public string Nameid { get; set; }
        public string Unique_name { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TaxId { get; set; }
        public string OfficialIdType { get; set; }
        public string Gender { get; set; }
        public string CuitCuil { get; set; }
        public object Role { get; set; }
        public int Nbf { get; set; }
        public int Exp { get; set; }
        public int Iat { get; set; }

    }
}
