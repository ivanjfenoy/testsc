﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppInsured.Models
{
    public class LandingCarouselItem
    {
        public string Id { get; set; }
        public FileImageSource Image { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public LandingCarouselItem()
        {
            OpenItemCommand = new Command(() => MessagingCenter.Send<LandingCarouselItem, LandingCarouselItem>(this, "OpenItem", this));
        }

        public ICommand OpenItemCommand { get; set; }
    }
}

