﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using System.Threading.Tasks;
using AppInsured.Resources;

namespace AppInsured.Models
{
    public class MenuItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public FileImageSource Icon { get; set; } = IconResources.Icon;
        public Type TargetType { get; set; }
    }
}
