﻿using System;
using Xamarin.Forms;

namespace AppInsured.Models
{
    public class OtherPolicy
    {
        public string Detail { get; set; }
        public Color Color{ get; set; }
        public bool IsBusy { get; set; }
    }
}
