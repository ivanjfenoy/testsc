﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Insured.Api.Dto.Queries;

namespace AppInsured.Models
{
    public class PolicyDTO : Policy
    {
        public bool IsBusy { get; set; }
    }
}
