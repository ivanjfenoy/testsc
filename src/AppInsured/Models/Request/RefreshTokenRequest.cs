﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppInsured.Models.Request
{
    public class RefreshTokenRequest
    {
        public string UserId { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
