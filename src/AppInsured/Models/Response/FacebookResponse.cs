﻿using System;
namespace AppInsured.Models.Response
{
    public class FacebookResponse
    {
        public string id { get; set; }
        public string email { get; set; }
    }
}
