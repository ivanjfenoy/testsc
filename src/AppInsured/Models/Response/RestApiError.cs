﻿using System;
using System.Collections.Generic;
using System.Net;

namespace AppInsured.Models.Response
{
    public class RestApiError
    {
        public string Message { get; set; }
        public List<string> Error { get; set; }
        public string StackTrace { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
