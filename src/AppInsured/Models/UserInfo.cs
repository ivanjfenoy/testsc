﻿using Newtonsoft.Json;

namespace AppInsured.Models
{
    public class UserInfo: BaseModel
    {
        public string UserId { get; set; }

        public string Email { get; set; }

        public string LastName { get; set; }
        
        public string FirstName { get; set; }

        public string Phone { get; set; }

        public string Password { get; set; }

        [JsonIgnore]
        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }

    }
}
