﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AppInsured.Observables
{
    public class ObservableGroupCollection<S, T> : ObservableCollection<T>
    {
        private readonly S _key;
        public List<T> List { get; set; }

        public ObservableGroupCollection(IGrouping<S, T> group)
            : base(group)
        {
            List = group.ToList();
            _key = group.Key;
        }

        public S Key
        {
            get { return _key; }
        }
    }
}
