﻿using Xamarin.Forms;

namespace AppInsured.Resources
{
    public class ColorResources
    {
        public static Color PrimaryColor = Color.FromHex("#0070CC");
        public static Color PrimaryColorLight = Color.FromHex("#0b192c");
        public static Color TextColorFloating = Color.FromHex("#000000");
        public static Color PlaceHolderFloating = Color.FromHex("#0070CC");
        public static Color PrimaryDarkColor = Color.FromHex("#1C72ED");
        public static Color Grey = Color.FromHex("#536279");
        public static Color GreyLight = Color.FromHex("#424F66");
        public static Color GreyHigh = Color.FromHex("#EDF0F5");
        public static Color GreyMedium= Color.FromHex("#8993A4");
        public static Color BackgrundGrey = Color.FromHex("#eef0f7");
        public static Color White = Color.FromHex("#FFFFFF");
        public static Color Black = Color.FromHex("#000000");
        public static Color Green = Color.FromHex("#1AAE00");
        public static Color Red = Color.FromHex("#D02F27");
        public static Color GreyLine = Color.FromHex("#d3d3d3");
        public static Color Transparent = Color.FromHex("#00000000");
        public static Color WhiteTransparent = Color.FromHex("#20FFFFFF");
        public static Color BlackTransparent = Color.FromHex("#80000000");
        public static Color PrimaryTransparent = Color.FromHex("#301672f2");
        public static Color ErrorColor = Color.FromHex("#FF5252");
        public static Color PinTextColor = Color.FromHex("#536279");
        public static Color LabelInputColor = Color.FromHex("#009C56");
        public static Color PolicyGreenColor = Color.FromHex("#00D095");
        public static Color PolicyYellowColor = Color.FromHex("#FBA344");
        public static Color PolicyRedColor = Color.FromHex("#DE350B");
        public static Color PolicyGreyColor = Color.FromHex("#C1C7D0");
        public static Color BackgroundLanding = Color.FromHex("#F9FAFB");
        public static string GetHexString(Color pColor)
        {
            var red = (int)(pColor.R * 255);
            var green = (int)(pColor.G * 255);
            var blue = (int)(pColor.B * 255);
            var alpha = (int)(pColor.A * 255);
            var hex = $"#{alpha:X2}{red:X2}{green:X2}{blue:X2}";

            return hex;
        }
    }

}
