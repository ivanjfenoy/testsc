﻿using Xamarin.Forms;

namespace AppInsured.Resources
{
    public class FontResources
    {
        public static readonly Font LargeLabel = Font.SystemFontOfSize(Device.GetNamedSize(NamedSize.Large, typeof(Label)));
        public static readonly Font SmallLabel = Font.SystemFontOfSize(Device.GetNamedSize(NamedSize.Small, typeof(Label)));
        public static readonly Font MediumLabel = Font.SystemFontOfSize(Device.GetNamedSize(NamedSize.Medium, typeof(Label)));
        public static readonly Font MicroLabel = Font.SystemFontOfSize(Device.GetNamedSize(NamedSize.Micro, typeof(Label)));

    }
}
