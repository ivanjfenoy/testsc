using Xamarin.Forms;

namespace AppInsured.Resources
{
    public class IconResources
    {
        public static FileImageSource LogoSC = new FileImageSource() { File = "logo_San_Cristobal.png" };
        public static FileImageSource Icon = new FileImageSource() { File = "Icon.png" };
        public static FileImageSource IcFb = new FileImageSource() { File = "facebook.png" };
        public static FileImageSource IcGoogle = new FileImageSource() { File = "Google.png" };
        public static FileImageSource IcReferidosBanner = new FileImageSource() { File = "referidos.png" };
        public static FileImageSource IcDDOCBanner = new FileImageSource() { File = "DDOCBanner.png" };
        public static FileImageSource IcRetiro = new FileImageSource() { File = "retiro.png" };
        public static FileImageSource IcTurismo = new FileImageSource() { File = "turismo.png" };
        public static FileImageSource IcServicioFinanciero = new FileImageSource() { File = "serviciofinanciero.png" };
    }
}
