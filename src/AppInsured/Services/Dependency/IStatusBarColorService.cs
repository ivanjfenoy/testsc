﻿using System.Threading.Tasks;

namespace AppInsured.Services.Dependency
{
    public interface IStatusBarColorService
    {
        void SetDarkTheme(string pColor);
        void SetLightTheme(string pColor);
    }
}
