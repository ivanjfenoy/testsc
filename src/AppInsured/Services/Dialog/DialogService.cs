﻿using Acr.UserDialogs;
using AppInsured.Resources;
using System.Threading.Tasks;

namespace AppInsured.Services.Dialog
{
    public class DialogService : IDialogService
    {
        public Task ShowAlertAsync(string message, string title, string buttonLabel)
        {
            return UserDialogs.Instance.AlertAsync(message, title, buttonLabel);
        }

        public void ShowToast(string message)
        {
            var toastConfig = new ToastConfig(message);
            toastConfig.SetDuration(3000);
            toastConfig.SetBackgroundColor(System.Drawing.Color.FromArgb((int)ColorResources.Red.A, (int)ColorResources.Red.G, (int)ColorResources.Red.B));
            UserDialogs.Instance.Toast(toastConfig);
        }

        public async Task<bool> ConfirmationMessage(string message)
        {
            return await UserDialogs.Instance.ConfirmAsync(message, null, StringResources.TextOk, StringResources.TextCancel);
        }

        public async Task<bool> ConfirmationMessage(string message, string confirmTextButton)
        {
            return await UserDialogs.Instance.ConfirmAsync(message, null, confirmTextButton, StringResources.TextCancel);
        }

        public IProgressDialog LoadingWithMessage(string message)
        {
            return UserDialogs.Instance.Loading(message, null, cancelText: null, show: true, maskType: null);
        }

        public async Task<string> ShowActionSheetAsync(string message, string[] actions)
        {
            return await UserDialogs.Instance.ActionSheetAsync(message, StringResources.TextCancel,null, null, actions);
        }
    }
}
