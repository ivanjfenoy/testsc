﻿using Acr.UserDialogs;
using System.Threading.Tasks;

namespace AppInsured.Services.Dialog
{
    public interface IDialogService
    {
        Task ShowAlertAsync(string message, string title, string buttonLabel);
        void ShowToast(string message);
        Task<bool> ConfirmationMessage(string message);
        Task<bool> ConfirmationMessage(string message, string confirmTextButton);
        IProgressDialog LoadingWithMessage(string message);
        Task<string> ShowActionSheetAsync(string message, string[] actions);
    }
}
