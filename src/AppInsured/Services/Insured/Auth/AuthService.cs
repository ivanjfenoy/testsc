﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AppCoreSDK.Models.Auth;
using AppCoreSDK.Models.Response;
using Core.Sdk.Lib;
using Insured.Api.Dto.Commands.Request;
using Insured.Api.Sdk;
using Newtonsoft.Json;

namespace AppInsured.Services.Insured.Auth
{
    public class AuthService: IAuthService
    {
        private AuthClient authClient;

        public AuthService()
        {
            authClient = new AuthClient(GlobalSetting.ClientInsured);
        }

        public async Task<SessionUser> LoginAsync(LoginCommand credentials, CancellationToken cancellationToken = default)
        {
            SessionUser userSession = new SessionUser();
            try
            {
                var jwt = await authClient.LoginAsync(credentials);
                userSession.AuthorizationToken = jwt.Auth_Token;
                userSession.RefreshToken = jwt.Refresh_Token;
                userSession.UserName = credentials.UserName;
                userSession.Password = credentials.Password;
                return userSession;
            }
            catch (RestApiException ex)
            {
                var error = JsonConvert.DeserializeObject<RestApiError>(ex.Message);
                error.StatusCode = ex.StatusCode;
                userSession.Error = error;
                return userSession;
            }
        }
    }
}
