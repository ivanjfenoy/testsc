﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AppCoreSDK.Models.Auth;
using Insured.Api.Dto.Commands.Request;

namespace AppInsured.Services.Insured.Auth
{
    public interface IAuthService
    {
        Task<SessionUser> LoginAsync(LoginCommand credentials, CancellationToken cancellationToken = default);
    }
}
