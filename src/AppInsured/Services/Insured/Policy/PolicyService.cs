﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AppInsured.Services.Settings;
using Insured.Api.Dto.Queries;
using Insured.Api.Sdk;
namespace AppInsured.Services.Insured.Policy
{
    public class PolicyService : IPolicyClient
    {
        private PolicyClient policyClient;
        private readonly ISettingsService _settingsService;

        public PolicyService(ISettingsService settingsService)
        {
            _settingsService = settingsService;
            policyClient = new PolicyClient(GlobalSetting.ClientInsured);
        }
        public Task<PolicyResponse> Get(CancellationToken cancellationToken = default)
        {
            SetAuth(_settingsService.Session.AuthorizationToken);
            return policyClient.Get();
        }

        public Task<BillingResponse> GetBillingByPolicy(PolicyBillingQuery query, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<PolicyBillingLinkResponse> GetPolicyBillingLink(PolicyBillingLinkQuery query, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<PolicyResponse> Refresh(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        private void SetAuth(string token)
        {
            RemoveAuth();
            string authToken = "Bearer " + token;
            GlobalSetting.ClientInsured.DefaultRequestHeaders.Add("Authorization", authToken);
            policyClient = new PolicyClient(GlobalSetting.ClientInsured);
        }

        private void RemoveAuth()
        {
            if (GlobalSetting.ClientInsured.DefaultRequestHeaders.Contains("Authorization"))
            {
                GlobalSetting.ClientInsured.DefaultRequestHeaders.Remove("Authorization");
            }
        }
    }
}
