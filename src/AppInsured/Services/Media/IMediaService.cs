﻿using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppInsured.Services.Media
{
    public interface IMediaService
    {
        Task<MediaFile> SelectPhoto();
        Task<MediaFile> TakePhoto();
        Task<bool> CheckPermissionslibraryAsync();
        Task<bool> CheckPermissionsCameraAsync();
    }
}
