﻿using AppInsured.Resources;
using AppInsured.Services.Dialog;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppInsured.Services.Media
{
    public class MediaService: IMediaService
    {
        private IDialogService _dialogService;

        public MediaService(IDialogService dialogService)
        {
            _dialogService = dialogService;
        }

        public async Task<MediaFile> SelectPhoto()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                //await _dialogService.ShowAlertAsync(StringResources.ErrorNoCamera, StringResources.AppName, StringResources.TextOk);
                return null;
            }

            var file = await CrossMedia.Current.PickPhotoAsync();
            return file;
        }

        public async Task<MediaFile> TakePhoto()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                //await _dialogService.ShowAlertAsync(StringResources.ErrorNoCamera, StringResources.AppName, StringResources.TextOk);
                return null;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                PhotoSize = PhotoSize.Custom,
                CustomPhotoSize = 50,
                CompressionQuality = 80
            });

            return file;
        }
        public async Task<bool> CheckPermissionsCameraAsync()
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                    {
                       // await _dialogService.ShowAlertAsync(StringResources.TextSelectPhotoMethod, "Permissions are needed to access the camera", "OK");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);

                    if (results.ContainsKey(Permission.Camera))
                    {
                        status = results[Permission.Camera];
                    }
                }

                if (status == PermissionStatus.Granted)
                {
                    return true;
                }
                else if (status != PermissionStatus.Unknown)
                {
                    //await _dialogService.ShowAlertAsync(StringResources.TextSelectPhotoMethod, "You deny permission to access the camera, please enable it to access", "OK");
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public async Task<bool> CheckPermissionslibraryAsync()
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
                    {
                        //await _dialogService.ShowAlertAsync(StringResources.TextSelectPhotoMethod, "Permissions are needed to access the photo library", "OK");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);

                    if (results.ContainsKey(Permission.Storage))
                    {
                        status = results[Permission.Storage];
                    }
                }

                if (status == PermissionStatus.Granted)
                {
                    return true;
                }
                else if (status != PermissionStatus.Unknown)
                {
                    //await _dialogService.ShowAlertAsync(StringResources.TextSelectPhotoMethod, "You deny permission to access the photo library, please enable it to access", "OK");
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
