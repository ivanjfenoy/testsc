﻿using AppInsured.ViewModels.Base;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppInsured.Services.Navigation
{
    public interface INavigationService
    {
        ViewModelBase PreviousPageViewModel { get; }

        Task InitializeAsync();

        Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase;

        Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase;

        Task NavigateToAsync<TViewModel>(object parameter, object parameter2) where TViewModel : ViewModelBase;

        Task NavigateToPageAsync(Page page);

        Task RemoveLastFromBackStackAsync();

        Task RemoveBackStackAsync();

        Task OnBackAsync();

        Task NavigateToPopUpAsync(PopupPage page);

        Task CloseAllPopUpPageAsync();

        Task CloseAllLoadingPageAsync();
    }

}
