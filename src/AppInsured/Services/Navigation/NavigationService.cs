﻿using AppInsured.Resources;
using AppInsured.Services.Settings;
using AppInsured.ViewModels;
using AppInsured.ViewModels.Base;
using AppInsured.Views;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppInsured.Services.Navigation
{
    public class NavigationService : INavigationService
    {
        private readonly ISettingsService _settingsService;

        public ViewModelBase PreviousPageViewModel
        {
            get
            {
                var mainPage = Application.Current.MainPage as CustomNavigationView;
                var viewModel = mainPage.Navigation.NavigationStack[mainPage.Navigation.NavigationStack.Count - 2].BindingContext;
                return viewModel as ViewModelBase;
            }
        }

        public NavigationService(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public Task InitializeAsync()
        {
            return NavigateToAsync<SplashViewModel>();
        }

        public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), null);
        }

        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter);
        }

        public Task NavigateToAsync<TViewModel>(object parameter, object parameter2) where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter, parameter2);
        }

        public Task RemoveLastFromBackStackAsync()
        {
            var mainPage = Application.Current.MainPage as CustomNavigationView;

            if (mainPage != null)
            {
                mainPage.Navigation.RemovePage(
                    mainPage.Navigation.NavigationStack[mainPage.Navigation.NavigationStack.Count - 2]);
            }

            return Task.FromResult(true);
        }

        public Task OnBackAsync()
        {
            var mainPage = Application.Current.MainPage as CustomNavigationView;

            if (mainPage != null)
            {
                mainPage.Navigation.PopAsync();
            }

            return Task.FromResult(true);
        }

        public Task RemoveBackStackAsync()
        {
            var mainPage = Application.Current.MainPage as CustomNavigationView;

            if (mainPage != null)
            {
                if (mainPage.Navigation.NavigationStack.Count > 1)
                {
                    for (int i = 0; i < mainPage.Navigation.NavigationStack.Count; i++)
                    {
                        var page = mainPage.Navigation.NavigationStack[0];
                        mainPage.Navigation.RemovePage(page);
                    }
                }
            }

            return Task.FromResult(true);
        }

        private async Task InternalNavigateToAsync(Type viewModelType, object parameter)
        {
            Page page = await SetUpPage(viewModelType, parameter);

            await (page.BindingContext as ViewModelBase).InitializeAsync(parameter);
        }

        private async Task InternalNavigateToAsync(Type viewModelType, object parameter, object parameter2)
        {
            Page page = await SetUpPage(viewModelType, parameter);
            await (page.BindingContext as ViewModelBase).InitializeAsync(parameter, parameter2);
        }

        private async Task<Page> SetUpPage(Type viewModelType, object parameter)
        {
            Page page = CreatePage(viewModelType, parameter);

            if (page is PopupPage)
            {
                var mainPage = Application.Current.MainPage as CustomNavigationView;
                if (mainPage != null)
                {
                    await mainPage.Navigation.PushPopupAsync((PopupPage)page);
                }
            }
            else
            {
                var navigationPage = Application.Current.MainPage as CustomNavigationView;
                if (navigationPage != null)
                {
                    await navigationPage.PushAsync(page);
                }
                else
                {
                    Application.Current.MainPage = new CustomNavigationView(page);
                }
            }

            return page;
        }

        public async Task NavigateToPageAsync(Page page)
        {
            var navigationPage = Application.Current.MainPage as CustomNavigationView;
            if (navigationPage != null)
            {
                await navigationPage.PushAsync(page);
            }
            else
            {
                Application.Current.MainPage = new CustomNavigationView(page);
            }
        }

        private Type GetPageTypeForViewModel(Type viewModelType)
        {
            var viewName = viewModelType.FullName.Replace("Model", string.Empty);
            var viewModelAssemblyName = viewModelType.GetTypeInfo().Assembly.FullName;
            var viewAssemblyName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}", viewName, viewModelAssemblyName);
            var viewType = Type.GetType(viewAssemblyName);
            return viewType;
        }

        private Page CreatePage(Type viewModelType, object parameter)
        {
            Type pageType = GetPageTypeForViewModel(viewModelType);
            if (pageType == null)
            {
                throw new Exception($"Cannot locate page type for {viewModelType}");
            }

            Page page = Activator.CreateInstance(pageType, false) as Page;
            return page;
        }

        public async Task NavigateToPopUpAsync(PopupPage page)
        {
            var mainPage = Application.Current.MainPage as CustomNavigationView;

            if (mainPage != null)
            {
                await mainPage.Navigation.PushPopupAsync(page);
            }
        }

        public async Task CloseAllPopUpPageAsync()
        {
            var mainPage = Application.Current.MainPage as CustomNavigationView;

            if (mainPage != null)
            {
                await mainPage.Navigation.PopPopupAsync();
            }
        }

        public async Task CloseAllLoadingPageAsync()
        {
            try
            {
                var mainPage = Application.Current.MainPage as CustomNavigationView;
                foreach (PopupPage p in PopupNavigation.Instance.PopupStack)
                {
                    if (p is LoadingView) await PopupNavigation.Instance.RemovePageAsync(p);
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
