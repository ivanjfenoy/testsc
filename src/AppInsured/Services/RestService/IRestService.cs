﻿using System;
using System.Threading.Tasks;
using AppInsured.Models.Response;
using Refit;
using Xamarin.Auth;

namespace AppInsured.Services.RestService
{
    public interface IFacebookApi
    {
        [Get("/v4.0/me?{data}")]
        Task<FacebookResponse> FaceBookProfile(string data);
    }
}
