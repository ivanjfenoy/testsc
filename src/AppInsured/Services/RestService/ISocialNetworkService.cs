﻿using System;
using System.Threading.Tasks;
using AppCoreSDK.Models.Auth;
using Xamarin.Auth;

namespace AppInsured.Services.RestService
{
    public interface ISocialNetworkService
    {
        Task<ExternalAuthQuery> GoogleProfile(ExternalAuthQuery externalAuthQuery, Account account);
        Task<ExternalAuthQuery> FaceBookProfile(ExternalAuthQuery externalAuthQuery);
    }
}
