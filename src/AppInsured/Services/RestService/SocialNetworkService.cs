﻿using System;
using System.IO;
using System.Threading.Tasks;
using AppCoreSDK.Models.Auth;
using AppInsured.Models.Response;
using AppInsured.Services.Settings;
using Newtonsoft.Json;
using Refit;
using Xamarin.Auth;

namespace AppInsured.Services.RestService
{
    public class SocialNetworkService : ISocialNetworkService
    {
        public async Task<AppCoreSDK.Models.Auth.ExternalAuthQuery> FaceBookProfile(AppCoreSDK.Models.Auth.ExternalAuthQuery externalAuthQuery)
        {
            try
            {
                var data = "access_token=" + externalAuthQuery.AccessToken + "&fields=name%2Cemail%2Cpicture%2Cfirst_name%2Clast_name&method=get&pretty=0&sdk=joey&suppress_http_code=1";
                FacebookResponse facebookResponse = await Refit.RestService.For<IFacebookApi>(GlobalSetting.FacebookUrlApi).FaceBookProfile(data);
                externalAuthQuery.Id = facebookResponse.id;
                externalAuthQuery.Email = facebookResponse.email;
            }
            catch (ApiException ex)
            {

            }
            return externalAuthQuery;
        }

        public async Task<ExternalAuthQuery> GoogleProfile(ExternalAuthQuery externalAuthQuery, Account account)
        {
            try
            {
                var request = new OAuth2Request("GET", new Uri(GlobalSetting.UserInfoUrl), null, account);
                var response = await request.GetResponseAsync();
                if (response != null)
                {
                    string userJson = await response.GetResponseTextAsync();
                    var googleResponse = JsonConvert.DeserializeObject<GoogleResponse>(userJson);
                    externalAuthQuery.Id = googleResponse.Id;
                    externalAuthQuery.Email = googleResponse.Email;
                }
            }
            catch (Exception ex)
            {

            }
            return externalAuthQuery;

        }
    }
}
