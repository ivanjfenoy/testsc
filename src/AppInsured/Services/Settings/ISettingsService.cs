using AppInsured.Models;
using AppCoreSDK.Models.Auth;
using System.Threading.Tasks;

namespace AppInsured.Services.Settings
{
    public interface ISettingsService
    {
        UserInfo UserInfo { get; set; }
        SessionUser Session { get; set; }
        string DeepLink { get; set; }
        bool GetValueOrDefault(string key, bool defaultValue);
        string GetValueOrDefault(string key, string defaultValue);
        Task AddOrUpdateValue(string key, bool value);
        Task AddOrUpdateValue(string key, string value);
    }
}
