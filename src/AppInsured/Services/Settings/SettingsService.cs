using AppInsured.Models;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using AppCoreSDK.Models.Auth;

namespace AppInsured.Services.Settings
{
    public class SettingsService : ISettingsService
    {
        #region Setting Constants

        IDictionary<string, object> _settings = new Dictionary<string, object>();

        private const string IdUserInfo = "user_info";
        private const string IdDeepLink = "deeplink";
        private const string IdSession = "session";
        #endregion

        #region Settings Properties

        public UserInfo UserInfo
        {
            get => JsonConvert.DeserializeObject<UserInfo>(GetValueOrDefault(IdUserInfo, ""));
            set => AddOrUpdateValue(IdUserInfo, JsonConvert.SerializeObject(value));
        }

        public SessionUser Session
        {
            get => JsonConvert.DeserializeObject<SessionUser>(GetValueOrDefault(IdSession, ""));
            set => AddOrUpdateValue(IdSession, JsonConvert.SerializeObject(value));
        }

        public string DeepLink
        {
            get => GetValueOrDefault(IdDeepLink, string.Empty);
            set => AddOrUpdateValue(IdDeepLink, value);
        }
        #endregion

        #region Public Methods

        public Task AddOrUpdateValue(string key, bool value) => AddOrUpdateValueInternal(key, value);
        public Task AddOrUpdateValue(string key, string value) => AddOrUpdateValueInternal(key, value);
        public bool GetValueOrDefault(string key, bool defaultValue) => GetValueOrDefaultInternal(key, defaultValue);
        public string GetValueOrDefault(string key, string defaultValue) => GetValueOrDefaultInternal(key, defaultValue);

        #endregion

        #region Internal Implementation

        async Task AddOrUpdateValueInternal<T>(string key, T value)
        {
            if (value == null)
            {
                await Remove(key);
            }

            Xamarin.Forms.Application.Current.Properties[key] = value;
            try
            {
                await Xamarin.Forms.Application.Current.SavePropertiesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to save: " + key, " Message: " + ex.Message);
            }
        }

        T GetValueOrDefaultInternal<T>(string key, T defaultValue = default(T))
        {
            object value = null;
            if (Xamarin.Forms.Application.Current.Properties.ContainsKey(key))
            {
                value = Xamarin.Forms.Application.Current.Properties[key];
            }
            return null != value ? (T)value : defaultValue;
        }

        async Task Remove(string key)
        {
            try
            {
                if (Xamarin.Forms.Application.Current.Properties[key] != null)
                {
                    Xamarin.Forms.Application.Current.Properties.Remove(key);
                    await Xamarin.Forms.Application.Current.SavePropertiesAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to remove: " + key, " Message: " + ex.Message);
            }
        }

        #endregion
    }
}
