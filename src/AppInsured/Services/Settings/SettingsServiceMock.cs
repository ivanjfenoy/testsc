﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppInsured.Models;
using AppCoreSDK.Models.Auth;
using Newtonsoft.Json;

namespace AppInsured.Services.Settings
{
    public class SettingsServiceMock : ISettingsService
    {
        #region Setting Constants

        IDictionary<string, object> _settings = new Dictionary<string, object>();

        private const string IdUserInfo = "user_info";
        private const string IdSession = "session";
        private const string IdDeepLink = "deeplink";

        #endregion

        #region Settings Properties

        public UserInfo UserInfo
        {
            get => JsonConvert.DeserializeObject<UserInfo>(GetValueOrDefault(IdUserInfo, ""));
            set => AddOrUpdateValue(IdUserInfo, JsonConvert.SerializeObject(value));
        }

        public SessionUser Session
        {
            get => JsonConvert.DeserializeObject<SessionUser>(GetValueOrDefault(IdSession, ""));
            set => AddOrUpdateValue(IdSession, JsonConvert.SerializeObject(value));
        }

        public string DeepLink
        {
            get => GetValueOrDefault(IdDeepLink, string.Empty);
            set => AddOrUpdateValue(IdDeepLink, value);
        }

        #endregion

        public Task AddOrUpdateValue(string key, bool value)
        {
            throw new NotImplementedException();
        }

        public Task AddOrUpdateValue(string key, string value)
        {
            throw new NotImplementedException();
        }

        public bool GetValueOrDefault(string key, bool defaultValue)
        {
            return defaultValue;
        }

        public string GetValueOrDefault(string key, string defaultValue)
        {
            return defaultValue;
        }
    }
}
