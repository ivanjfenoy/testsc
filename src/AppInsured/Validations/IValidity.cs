﻿namespace AppInsured.Validations
{
    public interface IValidity
    {
        bool IsValid { get; set; }
    }
}
