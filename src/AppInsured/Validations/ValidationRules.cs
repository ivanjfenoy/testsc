﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace AppInsured.Validations
{
    public class Validations
    {
        const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
        @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        const string passRegex = @"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$";

        const string phoneRegex = @"^[0-9]{13}$";

        public static bool ValidateEmail(string email)
        {
            return Regex.IsMatch(email, emailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }

        public static bool ValidatePassword(string pass)
        {
            return Regex.IsMatch(pass, passRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }

        public static bool ValidatePhone(string phone)
        {
            return Regex.IsMatch(phone, phoneRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
    }

    public class IsNotNullOrEmptyRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }

            var str = value as string;

            return !string.IsNullOrWhiteSpace(str);
        }
    }

    public class ValidEmailRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
        @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }
            var str = value as string;

            return Regex.IsMatch(str, emailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
    }

    public class ValidPasswordRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        const string passRegex = @"^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d]{8,}$";

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }
            var str = value as string;

            return Regex.IsMatch(str, passRegex);
        }
    }

    public class ValidLoginUserNameRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
        @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
        const string cuilRegex = @"^([0-9]{11}|[0-9]{2}-[0-9]{ 8}-[0-9]{1})$";
        const string dniRegex = @"[0-9]{7,8}";

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }
            var str = value as string;

            return Regex.IsMatch(str, emailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)) ||
                Regex.IsMatch(str, cuilRegex) || Regex.IsMatch(str, dniRegex);
        }
    }

    public class ValidDniCuilRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        const string cuilRegex = @"^([0-9]{11}|[0-9]{2}-[0-9]{ 8}-[0-9]{1})$";
        const string dniRegex = @"[0-9]{7,8}";

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }
            var str = value as string;

            return Regex.IsMatch(str, cuilRegex) || Regex.IsMatch(str, dniRegex);
        }
    }

    public class ValidNumberPhoneRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        const string passRegex = @"^[0-9]{13}$";

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }
            var str = value as string;

            return Regex.IsMatch(str, passRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
    }

    public class ValidLength<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        public int Length { get; set; }

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }
            var str = value as string;

            return str.Length >= Length;
        }
    }


}
