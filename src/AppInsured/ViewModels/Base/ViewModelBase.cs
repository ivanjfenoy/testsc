using Acr.UserDialogs;
using AppInsured.Resources;
using AppInsured.Services.Dialog;
using AppInsured.Services.Navigation;
using AppInsured.Services.Settings;
using AppInsured.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;
using AppInsured.Crosscutting.Exceptions;
using Refit;
using System.Diagnostics;
using Core.Sdk.Lib;
using AppInsured.Models.Response;
using AppCoreSDK.Models.Auth;
using AppInsured.Models.Request;
using Insured.Api.Sdk;
using AppCoreSDK.Crosscutting;
using AppCoreSDK;
using System.Threading;

namespace AppInsured.ViewModels.Base
{
    public abstract class ViewModelBase : ExtendedBindableObject
    {
        #region Members
        public event PropertyChangedEventHandler PropertyChanged;

        private string title;
        private bool isBusy;

        private string busyMessage = StringResources.BusyMessage;
        protected INavigation Navigation => this.Page.Navigation;
        protected readonly IDialogService DialogService;
        protected readonly ISettingsService SettingService;
        protected readonly INavigationService NavigationService;

        #endregion

        #region Properties
        public Page Page { get; set; }
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        public IProgressDialog BusyDialog { get; private set; }

        public string BusyMessage
        {
            get { return busyMessage; }
            set
            {
                busyMessage = value;
                RaisePropertyChanged(() => BusyMessage);
            }
        }
        #endregion

        #region Constructor
        public ViewModelBase()
        {
            DialogService = App.Container.Resolve<IDialogService>();
            NavigationService = App.Container.Resolve<INavigationService>();
            SettingService = App.Container.Resolve<ISettingsService>();
        }
        #endregion

        #region Protected Methods
        //protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        //{
        //    if (EqualityComparer<T>.Default.Equals(field, value))
        //        return false;

        //    field = value;
        //    this.OnPropertyChanged(propertyName);
        //    return true;
        //}

        //protected virtual void OnPropertyChanged(string propertyName = null)
        //{
        //    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
        #endregion

        #region Public Methods
        public virtual Task InitializeAsync(object navigationData)
        {
            return Task.FromResult(false);
        }
        public virtual Task InitializeAsync(object navigationData, object navigationData2)
        {
            return Task.FromResult(false);
        }
        //public async Task<string> DisplayActionSheet(string title, string cancel, string destruction, params string[] buttons)
        //{
        //    return await this.Page.DisplayActionSheet(title, cancel, destruction, buttons);
        //}

        //public async Task<bool> DisplayAlert(string title, string message, string accept, string cancel)
        //{
        //    return await this.Page.DisplayAlert(title, message, accept, cancel);
        //}

        //public async Task DisplayAlert(string title, string message, string cancel)
        //{
        //    await DialogService.ShowAlertAsync(message, title, cancel);
        //}

        public async Task BusyAsync(Func<Task> action, string message = "Cargando...", bool showLoading = true)
        {
            var cts = new CancellationTokenSource();

            Device.BeginInvokeOnMainThread(() =>
            {
                this.BusyMessage = message;
                this.IsBusy = true;
                if (showLoading)
                {
                    //this.BusyDialog = DialogService.LoadingWithMessage(this.BusyMessage);
                    NavigationService.NavigateToPopUpAsync(new LoadingView(cts));
                }
            });
            try
            {
                await action();
            }
            catch (RestApiException ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    await RefreshTokenAsync(action);
                else
                    await DialogService.ShowAlertAsync(((ex == null) ? StringResources.GenericErrorMsg : Newtonsoft.Json.JsonConvert.DeserializeObject<RestApiError>(ex.Message).Error[0].ToString()), StringResources.AppTitle, StringResources.TextOk);
            }
            catch (ApiException ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    await RefreshTokenAsync(action);
                }
                else
                {
                    var content = ex.GetContentAsAsync<Dictionary<String, String>>();
                    await DialogService.ShowAlertAsync(((ex == null) ? StringResources.GenericErrorMsg : ex.Message), StringResources.AppTitle, StringResources.TextOk);
                    Debug.WriteLine(content.Status);
                }
               
            }
            catch (ServiceAuthenticationException ex)
            {
                await RefreshTokenAsync(action);
            }
            catch (Exception ex)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await DialogService.ShowAlertAsync(((ex == null) ? StringResources.GenericErrorMsg : ex.Message), StringResources.AppTitle, StringResources.TextOk);
                });
            }
            finally
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    this.IsBusy = false;
                    if (showLoading)
                    {
                        cts.Cancel();
                        //MessagingCenter.Send<ViewModelBase>(this, "CloseLoadingView");
                    }
                });
            }

        }

        public async Task RefreshTokenAsync(Func<Task> action)
        {

            try
            {
                if (SettingService.Session == null || string.IsNullOrEmpty(SettingService.Session.AuthorizationToken) || string.IsNullOrEmpty(SettingService.Session.RefreshToken))
                {
                    await Logout();
                    return;
                }

                var token = new AppCoreSDK.Models.Auth.RefreshTokenRequest()
                {
                    Refresh_Token = SettingService.Session.RefreshToken,
                    Auth_Token = SettingService.Session.AuthorizationToken,
                    ApplicationToken = GlobalSetting.ApplicationToken
                };

                var CoreApp = new CoreApp(Application.Current.MainPage);
                CoreApp.SetUpAppToken(GlobalSetting.ApplicationToken)
                    .SetUpIdentityHttpClient(GlobalSetting.ClientIdentity);

                AppCoreSDK.Models.Auth.RefreshTokenRequest resp = await CoreApp.RefreshToken(token);

                if (resp != null)
                {
                    SessionUser userSession = SettingService.Session;
                    userSession.AuthorizationToken = resp.Auth_Token;
                    userSession.RefreshToken = resp.Refresh_Token;
                    RememberSession(userSession);
                }
                await action();
            }
            catch (Exception ex)
            {
                await Logout();
            }
        }

        protected async Task Logout()
        {
            SettingService.UserInfo = null;

            //await NavigationService.RemoveBackStackAsync();
            //await NavigationService.NavigateToAsync<HomeViewModel>();
        }

        public void RememberSession(SessionUser userSession)
        {
            ForgotSession();
            SettingService.Session = userSession;
        }

        public void ForgotSession()
        {
            SettingService.Session = null;
        }
        #endregion
    }
}
