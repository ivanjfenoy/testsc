﻿
using AppInsured.Bootstrap;
using AppInsured.Services.Dependency;
using AppInsured.Services.Dialog;
using AppInsured.Services.Insured.Auth;
using AppInsured.Services.Insured.Policy;
using AppInsured.Services.Media;
using AppInsured.Services.Navigation;
using AppInsured.Services.RestService;
using AppInsured.Services.Settings;
using Autofac;
using Autofac.Core;
using AutoMapper;
using Insured.Api.Sdk;
using System;
using System.Globalization;
using System.Reflection;
//using TinyIoC;
using Xamarin.Forms;
using static AppInsured.GlobalSetting;

namespace AppInsured.ViewModels.Base
{

    public static class ViewModelLocator
    {
        //private static TinyIoCContainer _container;

        public static readonly BindableProperty AutoWireViewModelProperty =
            BindableProperty.CreateAttached("AutoWireViewModel", typeof(bool), typeof(ViewModelLocator), default(bool), propertyChanged: OnAutoWireViewModelChanged);

        public static bool GetAutoWireViewModel(BindableObject bindable)
        {
            return (bool)bindable.GetValue(ViewModelLocator.AutoWireViewModelProperty);
        }

        internal static void UpdateDependencies(object useMocks)
        {
            throw new NotImplementedException();
        }

        public static void SetAutoWireViewModel(BindableObject bindable, bool value)
        {
            bindable.SetValue(ViewModelLocator.AutoWireViewModelProperty, value);
        }

        public static bool UseMockService { get; set; }

        static ViewModelLocator()
        {
            //_container = new TinyIoCContainer();
            //// View models - by default, TinyIoC will register concrete classes as multi-instance.
            //_container.Register<HomeViewModel>();
            //_container.Register<MainViewModel>();
            //_container.Register<LandingViewModel>();
            //_container.Register<SplashViewModel>();

            //// Services - by default, TinyIoC will register interface registrations as singletons.
            //UpdateDependencies(false);
        }

        public static void UpdateDependencies(bool useMockServices)
        {
            //// Change injected dependencies
            //if (useMockServices)
            //{
            //    _container.Register<ISettingsService, SettingsServiceMock>();

            //    UseMockService = true;
            //}
            //else
            //{
            //    _container.Register<INavigationService, NavigationService>().AsSingleton();
            //    _container.Register<ISettingsService, SettingsService>().AsSingleton();
            //    _container.Register<IDialogService, DialogService>().AsSingleton();
            //    _container.Register<IDependencyService, Services.Dependency.DependencyService>();
            //    _container.Register<IMediaService, MediaService>();
            //    _container.Register<ISocialNetworkService, SocialNetworkService>().AsSingleton();
            //    _container.Register<IPolicyClient, PolicyService>().AsSingleton();
            //    _container.Register<IAuthService, AuthService>().AsSingleton();
            //    _container.Register<IMapper, Mapper>().AsSingleton();
            //    UseMockService = false;
            //}
        }

        //public static void RegisterSingleton<TInterface, T>() where TInterface : class where T : class, TInterface
        //{
        //    _container.Register<TInterface, T>().AsSingleton();
        //}

        //public static T Resolve<T>() where T : class
        //{
        //    return _container.Resolve<T>();
        //}

        private static void OnAutoWireViewModelChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as Element;
            if (view == null)
            {
                return;
            }

            var viewType = view.GetType();
            var viewName = viewType.FullName.Replace(".Views.", ".ViewModels.");
            var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
            var viewModelName = string.Format(CultureInfo.InvariantCulture, "{0}Model, {1}", viewName, viewAssemblyName);

            var viewModelType = Type.GetType(viewModelName);
            if (viewModelType == null)
            {
                return;
            }
            var viewModel = App.Container.Resolve(viewModelType);
            view.BindingContext = viewModel;
        }
    }
}
