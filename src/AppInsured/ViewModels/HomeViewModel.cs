﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.AppCenter.Crashes;
using Xamarin.Auth;
using Xamarin.Forms;
using AppInsured.ViewModels.Base;
using AppInsured.Resources;
using AppInsured.Services.Settings;
using AppInsured.Services.Dialog;
using AppInsured;
using AppInsured.Crosscutting.Configuration;
using Core.Sdk.Lib;
using Application = Xamarin.Forms.Application;
using AppInsured.Services.RestService;
using AppCoreSDK;
using AppCoreSDK.Models.Auth;
using AppCoreSDK.Crosscutting.EventsHandlers;
using AppCoreSDK.Models;
using System.Web;
using ExternalAuthQuery = AppCoreSDK.Models.Auth.ExternalAuthQuery;
using AppCoreSDK.Crosscutting.Extensions;
using AppInsured.Services.Insured.Auth;

namespace AppInsured.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {

        #region Members
        private readonly ISettingsService _settingsService;
        private readonly IDialogService _dialogService;
        private ISocialNetworkService _socialNetworkService;
        private IAuthService _authService;
        #endregion

        #region Properties

        public ICommand OpenSignUpCommand { protected set; get; }
        public ICommand CallLoginCommand { protected set; get; }
        public ICommand OnFacebookLoginSuccessCmd { get; set; }
        public ICommand OnFacebookLoginErrorCmd { get; set; }
        public ICommand OnGoogleLoginCmd { get; set; }

        public OAuth2Authenticator Authenticator { get; set; }
        public CoreApp CoreApp { get; set; }

        #endregion

        #region Constructor
        public HomeViewModel(ISettingsService settingsService, IDialogService dialogService, ISocialNetworkService socialNetworkService, IAuthService authService)
        {
            _settingsService = settingsService;
            _dialogService = dialogService;
            _socialNetworkService = socialNetworkService;
            _settingsService.UserInfo = null;
            _authService = authService;

            this.CallLoginCommand = new Command(async () => await OpenLoginPage());
            this.OpenSignUpCommand = new Command(async () => await OpenSignUpPage());
            OnFacebookLoginSuccessCmd = new Command<string>(async (authToken) => await OnLoginFacebookAsync(authToken));
            OnFacebookLoginErrorCmd = new Command<string>(
            (err) => DisplayAlert("Error", $"{ err }"));
            OnGoogleLoginCmd = new Command(() => OnLoginGoogle());

            void DisplayAlert(string title, string msg) =>
                (Application.Current as App).MainPage.DisplayAlert(title, msg, "OK");

            InitLoginGoogle();
        }
        #endregion

        #region Private Methods

        public void InitView()
        {
            if(CoreApp == null)
            {
                CoreApp = new CoreApp(Application.Current.MainPage);
                CoreApp.SetUpAppToken(GlobalSetting.ApplicationToken)
                    .SetUpIdentityHttpClient(GlobalSetting.ClientIdentity)
                    .SetUpInsuredHttpClient(GlobalSetting.ClientInsured)
                    .SetUpURLs(GlobalSetting.UrlTermsAndCondition, GlobalSetting.UrlPolicies)
                    .SetUpOnLoginCompletedDelegate(new LoginCompletedDelegate(LoginCompleted))
                    .SetUpOnLoginErrorDelegate(new LoginErrorDelegate(LoginError))
                    .SetUpOpenHelpDelegate(new OpenHelpDelegate(OpenHelp))
                    .SetUpPasswordChangedDelegate(new PasswordChangedDelegate(PasswordChangedAsync));
            }

            string link = _settingsService.DeepLink;
            if (!string.IsNullOrEmpty(link))
            {
                var uri = new Uri(link);
                string pwd = HttpUtility.ParseQueryString(uri.Query).Get("pdw");
                string token = HttpUtility.ParseQueryString(uri.Query).Get("token");
                var data = new IdentityAuthToken
                {
                    Pwd = pwd,
                    Token = token
                };
                if (link.Contains("recuperar-contrasena", StringComparison.OrdinalIgnoreCase))
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await CoreApp.LaunchNewPassword(data);
                    });
                }
                else if (link.Contains("activar-cuenta", StringComparison.OrdinalIgnoreCase))
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await CoreApp.LaunchActivateAccount(data);
                    });
                }
                _settingsService.DeepLink = string.Empty;
            }
        }

        public async void LoginCompleted(SessionUser session)
        {
            RememberSession(session);
            await NavigationService.NavigateToAsync<MainViewModel>();
            //await NavigationService.CloseAllLoadingPageAsync();
            //SessionUser userSession = new SessionUser();
            ////CALL BE

            //await this.BusyAsync(async () =>
            //{
            //    userSession = await _authService.LoginAsync(new Insured.Api.Dto.Commands.Request.LoginCommand
            //    {
            //        ApplicationToken = GlobalSetting.ApplicationToken,
            //        UserName = session.UserName,
            //        Password = session.Password
            //    });
            //}, "", true);
            ////await Task.Delay(500);

            //if (userSession.Error != null)
            //{
            //    await ValidateLoginStatusCode(userSession);
            //}
            //else
            //{
            //    if (string.IsNullOrEmpty(userSession.AuthorizationToken)) return;

            //    Claims claims = ClaimToken.GetClaims(userSession.AuthorizationToken);

            //    if (((List<string>)claims.Role).Contains(IdentityRole.IdentityVerify))
            //    //if (!((List<string>)claims.Role).Contains(IdentityRole.IdentityVerify))
            //    {
            //        RememberSession(userSession);

            //        var mainViewModel = ViewModelLocator.Resolve<MainViewModel>();
            //        mainViewModel.GetPage();
            //    }
            //    else
            //    {
            //        //VALIDACION IDENTIDAD
            //        await CoreApp.LaunchIdentityValidation(userSession);
            //    }
            //}
        }

        //private async Task ValidateLoginStatusCode(SessionUser userSession)
        //{
        //    switch (userSession.Error.StatusCode.GetHashCode())
        //    {
        //        case 401:
        //            //await NavigationService.OnBackAsync();
        //            Device.BeginInvokeOnMainThread(async () => {
        //                await DialogService.ShowAlertAsync(userSession.Error.Error[0], "Revisá tus datos", StringResources.TextOk);
        //            });
        //            break;
        //        case 409:
        //            //await Navigation.PushAsync(new EmailSendPage(userSession.Error.error[0]));
        //            break;
        //        case 423:
        //            await CoreApp.LaunchBlockUser("Usuario bloqueado");
        //            break;
        //        default:
        //            Device.BeginInvokeOnMainThread(async () => {
        //                await DialogService.ShowAlertAsync(userSession.Error.Error[0], "Revisá tus datos", StringResources.TextOk);
        //            });
        //            await CoreApp.LaunchLogin();
        //            break;
        //    }
        //}

        public async void LoginError(string error)
        {
            ForgotSession();
            await NavigationService.NavigateToAsync<MainViewModel>();
        }

        public async void OpenHelp(string message)
        {
            await DialogService.ShowAlertAsync("PAGINA DE AYUDA", StringResources.AppTitle, StringResources.TextOk);
            await NavigationService.NavigateToAsync<MainViewModel>();
        }

        public async Task OpenLoginPage()
        {
            await CoreApp.LaunchLogin();
        }

        private async Task OpenSignUpPage()
        {
            await CoreApp.LaunchSignUp();
        }

        public void PasswordChangedAsync(string message)
        {
            CoreApp.LaunchLogin();
        }

        public void InitLoginGoogle()
        {

            string clientId = null;
            string redirectUri = null;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    clientId = GlobalSetting.iOSClientId;
                    redirectUri = GlobalSetting.iOSRedirectUrl;
                    break;

                case Device.Android:
                    clientId = GlobalSetting.AndroidClientId;
                    redirectUri = GlobalSetting.AndroidRedirectUrl;
                    break;
                case "Test":
                    clientId = GlobalSetting.iOSClientId;
                    redirectUri = GlobalSetting.iOSRedirectUrl;
                    break;
            }
            Authenticator = new OAuth2Authenticator(
                clientId,
                null,
                GlobalSetting.Scope,new Uri(GlobalSetting.AuthorizeUrl),
                
                new Uri(redirectUri),
                new Uri(GlobalSetting.AccessTokenUrl),
                null,
                true);

            Authenticator.Completed += OnAuthGoogleCompleted;
            Authenticator.Error += OnAuthGoogleError;

            AuthenticationState.Authenticator = Authenticator;
        }

        private void OnLoginGoogle()
        {
            var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(Authenticator);
        }

        private async void OnAuthGoogleCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthGoogleCompleted;
                authenticator.Error -= OnAuthGoogleError;
            }
            if (e.IsAuthenticated)
            {
                string token = string.Empty;
                e.Account.Properties.TryGetValue("access_token", out token);
                ExternalAuthQuery externalAuthQuery = new ExternalAuthQuery()
                {
                    AccessToken = token,
                    LoginProvider = GlobalSetting.GoogleLoginProvider,
                    ApplicationToken = GlobalSetting.ApplicationToken
                };
                externalAuthQuery = await _socialNetworkService.GoogleProfile(externalAuthQuery, e.Account);
                await ExternalLogin(externalAuthQuery);
            }
        }

        private void OnAuthGoogleError(object sender, AuthenticatorErrorEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthGoogleCompleted;
                authenticator.Error -= OnAuthGoogleError;
            }

            Debug.WriteLine("Authentication error: " + e.Message);
        }

        private async Task OnLoginFacebookAsync(string token)
        {
            ExternalAuthQuery externalAuthQuery = new ExternalAuthQuery()
            {
                AccessToken = token,
                LoginProvider = GlobalSetting.FacebookLoginProvider,
                ApplicationToken = GlobalSetting.ApplicationToken
            };
            //externalAuthQuery = await _socialNetworkService.FaceBookProfile(externalAuthQuery);
            await ExternalLogin(externalAuthQuery);
        }

        private async Task ExternalLogin(ExternalAuthQuery externalAuthQuery)
        {
            if (this.IsBusy)
                return;
            await this.BusyAsync(async () =>
            {
                try
                {
                    await CoreApp.ExternalLogin(externalAuthQuery);
                }
                catch (RestApiException ex)
                {
                    //await NavigationService.NavigateToAsync<LoginErrorViewModel>(1);
                }
                catch (Exception ex)
                {
                    _settingsService.UserInfo = null;
                    _settingsService.Session = null;
                    //await NavigationService.NavigateToAsync<GenericErrorViewModel>();
                }
            }, StringResources.loginLoadingText, true);
        }
        #endregion
    }
}
