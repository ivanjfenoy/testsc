﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCoreSDK.Crosscutting.Extensions;
using AppCoreSDK.Services.Dialog;
using AppCoreSDK.Services.Identity.Auth;
using AppInsured.Crosscutting.Identifiers;
using AppInsured.Models;
using AppInsured.Models.Response;
using AppInsured.Resources;
using AppInsured.Services.Settings;
using AppInsured.ViewModels.Base;
using AutoMapper;
using Insured.Api.Dto.Queries;
using Insured.Api.Sdk;
using Microsoft.AppCenter.Analytics;
using Xamarin.Forms;

namespace AppInsured.ViewModels
{
    public class LandingViewModel : ViewModelBase
    {
        private IPolicyClient _policyService;
        private readonly ISettingsService _settingsService;
        private PolicyResponse policyResponse = new PolicyResponse();
        private ObservableCollection<LandingCarouselItem> carousel;
        //private IEnumerable<CustomPolicy> policies;
        public LandingCarouselItem CurrentItem { get; set; }
        private bool hasCar;
        private int heightPolicy = 200;
        private int heightOtherPolicy = 40;
        private ObservableCollection<OtherPolicy> otherPolicies;
        private ObservableCollection<PolicyDTO> policies;
        public bool HasCar
        {
            get { return hasCar; }
            set
            {
                hasCar = value;
                RaisePropertyChanged(() => HasCar);
            }
        }
        public int HeightPolicy
        {
            get
            {
                if(Policies != null && Policies.Count() > 0)
                {
                    return heightPolicy;
                }
                else
                {
                    return 0;
                }
            }
        }
        public int HeightOtherPolicy
        {
            get
            {
                if (OtherPolicies != null && OtherPolicies.Count() > 0)
                {
                    return heightOtherPolicy;
                }
                else
                {
                    return 0;
                }
            }
        }
        public ObservableCollection<LandingCarouselItem> Carousel
        {
            get
            {
                return carousel;
            }
            set
            {
                carousel = value;
                RaisePropertyChanged(() => Carousel);
            }
        }
        public ObservableCollection<OtherPolicy> OtherPolicies
        {
            get
            {
                return otherPolicies;
            }
            set
            {
                otherPolicies = value;
                RaisePropertyChanged(() => OtherPolicies);
            }
        }
        public ObservableCollection<PolicyDTO> Policies
        {
            get
            {
                return policies;
            }
            set
            {
                policies = value;
                RaisePropertyChanged(() => Policies);
                RaisePropertyChanged(() => HeightPolicy);
            }
        }
        public ICommand OptionCommand => new Command<string>(async (page) => await OpenOption(page));

        public LandingViewModel(IPolicyClient policyService, ISettingsService settingsService)
        {
             //this.IsBusy = true;
             //SetFakeData();
            _policyService = policyService;
            _settingsService = settingsService;
            this.Title = "Hola, " + ClaimToken.GetClaims(_settingsService.Session.AuthorizationToken).FirstName + " " + ClaimToken.GetClaims(_settingsService.Session.AuthorizationToken).LastName;

            GetPolicies();
            LandingCarousel();

        }
        public async Task GetPolicies()
        {
            await this.BusyAsync(async () =>
            {
                policyResponse = await _policyService.Get();
            }, "", true);

            Policies = App.mapper.Map<ObservableCollection<PolicyDTO>>(policyResponse.Policies.Where(x => x.StatusCode.Equals(StatusCode.Bound.ToString())).Take(6));
            Policies.ToList().ForEach(s => s.Description = "Póliza " + s.Description.ToLower());

            HasCar = policyResponse.Policies.Any(x => x.StatusCode.Equals(StatusCode.Bound.ToString()) && x.Branch == Branch.Car.ToString());
            CreateOtherPolicies();
            //this.IsBusy = false;
        }

        //private void SetFakeData()
        //{
        //    string title = "xxxxxxxxxxx";
        //    Policies = new ObservableCollection<PolicyDTO>(new ObservableCollection<PolicyDTO> {
        //        new PolicyDTO
        //        {
        //            Description = title,
        //            PolicyNumber = title,
        //            Detail = title,
        //            DetailLabel = title,
        //            StartDate = title,
        //            EndDate = title,
        //           IsBusy = true

        //        },
        //        new PolicyDTO
        //        {
        //            Description = title,
        //            PolicyNumber = title,
        //            Detail = title,
        //            DetailLabel = title,
        //            StartDate = title,
        //            EndDate = title,
        //            IsBusy = true
        //        }
        //    });

        //    ObservableCollection<OtherPolicy> items = new ObservableCollection<OtherPolicy>();
        //    items.Add(new OtherPolicy()
        //    {
        //        Detail = title,
        //        IsBusy = true
        //    });
        //    items.Add(new OtherPolicy()
        //    {
        //        Detail = title,
        //        IsBusy = true
        //    });
        //    items.Add(new OtherPolicy()
        //    {
        //        Detail = title,
        //        IsBusy = true
        //    });

        //    OtherPolicies = items;
        //}

        public async Task LandingCarousel()
        {
            ObservableCollection<LandingCarouselItem> items = new ObservableCollection<LandingCarouselItem>();

            items.Add(new LandingCarouselItem()
            {
                Id = ViewsType.Referred,
                Image = IconResources.IcReferidosBanner,
                Title = StringResources.TitleCarouselReferido,
                Description = StringResources.DescriptionCarouselReferido
            });

            items.Add(new LandingCarouselItem()
            {
                Id = ViewsType.Medic,
                Image = IconResources.IcDDOCBanner,
                Title = StringResources.TitleCarouselMedico,
                Description = StringResources.DescriptionCarouselMedico
            });

            Carousel = items;
        }

        private void CreateOtherPolicies()
        {
            int CountCanceledPolicy = policyResponse.Policies.Where(x => x.StatusCode.Equals(StatusCode.Canceled.ToString())).Count();
            int CountExpiredPolicy = policyResponse.Policies.Where(x => x.StatusCode.Equals(StatusCode.Expired.ToString())).Count();
            int CountScheduledPolicy = policyResponse.Policies.Where(x => x.StatusCode.Equals(StatusCode.Scheduled.ToString())).Count();

            ObservableCollection<OtherPolicy> items = new ObservableCollection<OtherPolicy>();

            if (CountCanceledPolicy > 0)
            {
                items.Add(new OtherPolicy()
                {
                    Color = ColorResources.PolicyRedColor,
                    Detail = string.Concat(CountCanceledPolicy.ToString(), (CountCanceledPolicy > 1) ? StringResources.TextCanceladas : StringResources.TextCancelada)
                });
            }

            if (CountScheduledPolicy > 0)
            {
                items.Add(new OtherPolicy()
                {
                    Color = ColorResources.PolicyYellowColor,
                    Detail = string.Concat(CountScheduledPolicy.ToString(), (CountScheduledPolicy > 1) ? StringResources.TextProgramadas : StringResources.TextProgramada)
                });
            }

            if (CountExpiredPolicy > 0)
            {
                items.Add(new OtherPolicy()
                {
                    Color = ColorResources.PolicyGreyColor,
                    Detail = string.Concat(CountExpiredPolicy.ToString(), (CountExpiredPolicy > 1) ? StringResources.TextNoVigentes : StringResources.TextNoVigente)
                });
            }

            OtherPolicies = items;

            //if (OtherPolicies.Count().Equals(0)) HeightOtherPolicy = 0;
        }

        private async Task OpenOption(string page)
        {
            Page displayPage = null;
            switch (page)
            {
                case ViewsType.Retirement:
                    await Xamarin.Essentials.Launcher.OpenAsync(new Uri("https://www.tufuturohoy.com/"));
                    break;
                case ViewsType.Tourism:
                    await Xamarin.Essentials.Launcher.OpenAsync(new Uri("https://www.sancristobalturismo.com.ar/institucional/"));
                    break;
                case ViewsType.Finance:
                    await Xamarin.Essentials.Launcher.OpenAsync(new Uri("https://www.sancristobalsf.com.ar/"));
                    break;
            }
            if (displayPage != null)
            {
                Analytics.TrackEvent("Ingreso pantalla " + displayPage.Title, new Dictionary<string, string> {
                       { "Tipo Acceso", "Usuario Logueado" },
                       { "Usuario", ClaimToken.GetClaims(_settingsService.Session.AuthorizationToken).OfficialIdType + " " + ClaimToken.GetClaims(_settingsService.Session.AuthorizationToken).TaxId}
                    });
            }
        }
    }
}
