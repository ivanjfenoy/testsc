﻿using System;
using AppInsured.Services.Settings;
using AppInsured.ViewModels.Base;
using AppInsured.Views;
using Xamarin.Forms;

namespace AppInsured.ViewModels
{
    public class MainViewModel: ViewModelBase
    {
        private readonly ISettingsService _settingsService;

        public MainViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public Page GetPage()
        {
            if (_settingsService.Session != null)
                return (Page)Activator.CreateInstance(typeof(LandingView));
            else
                return (Page)Activator.CreateInstance(typeof(HomeView));
        }
    }
}
