﻿using System;
using System.Threading.Tasks;
using AppInsured.Services.Settings;
using AppInsured.ViewModels.Base;
using Xamarin.Forms;

namespace AppInsured.ViewModels
{
    public class SplashViewModel : ViewModelBase
    {
        private readonly ISettingsService _settingsService;

        public SplashViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;

            SetMain();
        }

        public async Task SetMain()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await NavigationService.RemoveBackStackAsync();
                await NavigationService.NavigateToAsync<MainViewModel>();
            });
        }
    }
}
