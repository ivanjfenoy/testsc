﻿using AppInsured.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppInsured.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeView : ContentPage
    {
        public HomeView()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            ((HomeViewModel)BindingContext).InitView();
            base.OnAppearing();
        }
    }
}
