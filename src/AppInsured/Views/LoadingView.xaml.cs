﻿using AppInsured.ViewModels.Base;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppInsured.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoadingView : PopupPage
    {
		private bool CheckCancel = false;

        public CancellationTokenSource CancelToken { get; set; }

        public LoadingView(CancellationTokenSource cancelToken = null)
		{
			InitializeComponent();
			CancelToken = cancelToken;
			if(CancelToken != null)
            {
				CheckCancel = true;
				CheckCancelation();
            }

			MessagingCenter.Subscribe<ViewModelBase>(this, "CloseLoadingView", async (sender) =>
			{
				await PopupNavigation.Instance.PopAsync();
			});
		}

		private async Task CheckCancelation()
        {
			await Task.Delay(500);
			while (CheckCancel)
			{
                if (CancelToken.IsCancellationRequested)
				{
					//await PopupNavigation.Instance.PopAsync();
					foreach (PopupPage p in PopupNavigation.Instance.PopupStack)
					{
						if (p is LoadingView) await PopupNavigation.Instance.RemovePageAsync(p);
					}

					CheckCancel = false;
				}
				await Task.Delay(500);
			}
		}
	}
}