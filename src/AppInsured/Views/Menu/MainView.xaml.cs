﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppInsured.Models;
using AppInsured.Resources;
using AppInsured.Services.Settings;
using AppInsured.ViewModels;
using AppInsured.ViewModels.Base;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MenuItem = AppInsured.Models.MenuItem;

namespace AppInsured.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainView : FlyoutPage
    {
        public MainView()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
            try
            {
                Detail = new NavigationPage(((MainViewModel)BindingContext).GetPage())
                {
                    BarBackgroundColor = ColorResources.PrimaryDarkColor,
                    BarTextColor = Color.White
                };
            }
            catch (Exception ex)
            {
                Analytics.TrackEvent("Error Set MainView " + ex.StackTrace);
                Crashes.TrackError(ex);
            }
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MenuItem;
            MasterPage.ListView.SelectedItem = null;
            if (item != null)
            {
                Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType))
                {
                    BarBackgroundColor = ColorResources.PrimaryDarkColor,
                    BarTextColor = Color.White
                };
                MasterPage.ListView.SelectedItem = null;
                IsPresented = false;
            }
        }
    }
}
