﻿
using AppInsured.ViewModels.Base;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppInsured.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashView : ContentPage
	{
		public SplashView()
		{
			InitializeComponent();
		}
	}
}